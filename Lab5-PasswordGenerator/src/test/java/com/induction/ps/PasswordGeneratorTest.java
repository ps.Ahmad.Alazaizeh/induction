package com.induction.ps;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PasswordGeneratorTest {
    @Test
    public void givenInvalidPassword_whenGenerated_thenThrowException(){
        PasswordGenerator PG = new PasswordGenerator();
        String password = PG.generate();
        Assertions.assertNotNull(password, "Invalid! password is Null!");
        Assertions.assertEquals(8, password.length(), "Invalid! Length is not 8");
        Assertions.assertTrue(password.matches(".*[A-Z].*[A-Z].*"), "Invalid password!");
        Assertions.assertTrue(password.matches(".*\\d.*\\d.*\\d.*\\d.*"), "Invalid password!");
        Assertions.assertTrue(password.matches(".*[_$#%].*[_$#%].*"), "Invalid password!");
    }

    @Test
    public void givenValidPasswordCheckIfUnique_whenGenerated_thenSuccess(){
        PasswordGenerator PG = new PasswordGenerator();
        String tempPassword = PG.generate();
        for (int i = 0; i < 100000; i++) {
            String tempPasswordTwo = PG.generate();
            if ((tempPassword.matches(".*\\d{4}.*")) || (tempPassword.matches(".*[A-Z]{2}.*")) || (tempPassword.matches(".*[_$#%]{2}.*"))){
                Assertions.fail("Invalid! Same group of chars/numbers/symbols are in a row");
            }
            Assertions.assertNotEquals(tempPassword, tempPasswordTwo, "Invalid! Not unique password");
            tempPassword = tempPasswordTwo;
        }
    }
}
