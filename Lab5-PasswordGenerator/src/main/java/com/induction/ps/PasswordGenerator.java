package com.induction.ps;

import java.util.*;

public class PasswordGenerator {

    Random random = new Random();

    public String generate() {
        ArrayList<String> generatedPassword = new ArrayList<>();
        generatedPassword.addAll(addNumbers(4));
        generatedPassword.addAll(addUpperCaseLetters( 2));
        String[] symbols = {"_", "$", "#", "%"};
        generatedPassword.addAll(addSymbols(symbols, 2));
        return checkIfShuffled(generatedPassword);
    }

    private String checkIfShuffled(ArrayList<String> generatedPassword) {
        String result = arrayListToString(generatedPassword);
        while ((result.matches(".*\\d{4}.*")) || (result.matches(".*[A-Z]{2}.*")) || (result.matches(".*[_$#%]{2}.*"))){
            shuffle(generatedPassword);
            result = arrayListToString(generatedPassword);
        }
        return result;
    }

    private ArrayList<String> addSymbols(String[] symbols, int count) {
        ArrayList<String> password = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            password.add(symbols[random.nextInt(0, 3)]);
        }
        return password;
    }

    private ArrayList<String> addUpperCaseLetters(int count) {
        ArrayList<String> password = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            password.add(String.valueOf((char) (random.nextInt(26) + 'a')).toUpperCase());
        }
        return password;
    }

    private ArrayList<String> addNumbers(int count) {
        ArrayList<String> password = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            password.add(String.valueOf(random.nextInt(0, 10)));
        }
        return password;
    }

    private String arrayListToString(ArrayList<String> arrayList) {
        StringBuilder result = new StringBuilder();
        for (String s : arrayList) {
            result.append(s);
        }
        return result.toString();
    }

    private void shuffle(ArrayList<String> generatedPassword) {
        Random random = new Random();
        for (int i = 0; i < random.nextInt(5); i++) {
            Collections.shuffle(generatedPassword);
        }
    }
}
