package org.progressoft.induction;

public class LowerTriangularCopyBehaviour implements CopyBehaviour{
    @Override
    public Matrix copySquareMatrix(Matrix matrix) {
        int[][] resultArray = new int[matrix.getRows()][matrix.getCols()];
        for (int i = 0; i < matrix.getRows(); i++) {
            for (int j = 0; j < matrix.getCols(); j++) {
                if (j > i) {
                    resultArray[i][j] = 0;
                } else {
                    resultArray[i][j] = matrix.getValue(i, j);
                }
            }
        }
        return new Matrix(resultArray);
    }
}
