package org.progressoft.induction;

public interface CopyBehaviour {
    Matrix copySquareMatrix(Matrix matrix);
}
