package org.progressoft.induction;

public class MatrixUtility {

    CopyBehaviour copyBehaviour;

    public MatrixUtility(CopyBehaviour cb){
        copyBehaviour = cb;
    }

    public MatrixUtility(){

    }

    public Matrix sum(Matrix leftMatrix, Matrix rightMatrix) {
        checkIfEqualSizes(leftMatrix, rightMatrix);
        int[][] result = new int[leftMatrix.getRows()][leftMatrix.getCols()];

        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                result[i][j] = leftMatrix.getValue(i, j) + rightMatrix.getValue(i, j);
            }
        }
        return new Matrix(result);
    }

    public Matrix scalarMultiplication(int multiplier, Matrix matrix) {
        int[][] resultArray = new int[matrix.getRows()][matrix.getCols()];
        for (int i = 0; i < matrix.getRows(); i++) {
            for (int j = 0; j < matrix.getCols(); j++) {
                resultArray[i][j] = multiplier * matrix.getValue(i, j);
            }
        }
        return new Matrix(resultArray);
    }

    public Matrix transportation(Matrix matrix) {
        int[][] resultArray = new int[matrix.getCols()][matrix.getRows()];
        for (int i = 0; i < matrix.getCols(); i++) {
            for (int j = 0; j < matrix.getRows(); j++) {
                resultArray[i][j] = matrix.getValue(j, i);
            }
        }
        return new Matrix(resultArray);
    }

    public Matrix multiplication(Matrix leftMatrix, Matrix rightMatrix) {
        checkMatricesSizesForMultiplication(leftMatrix, rightMatrix);
        int[][] resultArray = new int[leftMatrix.getRows()][rightMatrix.getCols()];
        for (int i = 0; i < leftMatrix.getRows(); i++) {
            for (int j = 0; j < rightMatrix.getCols(); j++) {
                for (int k = 0; k < leftMatrix.getCols(); k++) {
                    resultArray[i][j] += leftMatrix.getValue(i, k) * rightMatrix.getValue(k, j);
                }
            }
        }
        return new Matrix(resultArray);
    }

    public Matrix subMatrix(Matrix matrix, int rowToRemoveIndex, int colToRemoveIndex) {
        checkIfIndexOutOfBoundary(matrix, rowToRemoveIndex);
        checkIfIndexOutOfBoundary(matrix, colToRemoveIndex);
        int[][] resultArray = new int[matrix.getRows() - 1][matrix.getCols() - 1];
        int targetedRow = -1;
        for (int i = 0; i < matrix.getRows(); i++) {
            if (i == rowToRemoveIndex) {
                continue;
            }
            targetedRow++;
            int targetedCol = -1;
            for (int j = 0; j < matrix.getCols(); j++) {
                if (j == colToRemoveIndex) {
                    continue;
                }
                targetedCol++;
                resultArray[targetedRow][targetedCol] = matrix.getValue(i, j);
            }
        }
        return new Matrix(resultArray);
    }

    public Matrix subMatrix(Matrix matrix, int toRemoveIndex, boolean isRow) {
        checkIfIndexOutOfBoundary(matrix, toRemoveIndex);
        int rows = matrix.getRows();
        int cols = matrix.getCols();
        if (isRow) {
            rows--;
        } else {
            cols--;
        }
        int[][] resultArray = new int[rows][cols];
        int targetedRow = -1;
        for (int i = 0; i < matrix.getRows(); i++) {
            if (i == toRemoveIndex && isRow)
                continue;
            targetedRow++;
            int targetedCol = -1;
            for (int j = 0; j < matrix.getCols(); j++) {
                if (j == toRemoveIndex && !isRow)
                    continue;
                targetedCol++;
                resultArray[targetedRow][targetedCol] = matrix.getValue(i, j);
            }
        }
        return new Matrix(resultArray);
    }

    private void checkIfIndexOutOfBoundary(Matrix matrix, int toRemoveIndex) {
        if ((toRemoveIndex > matrix.getCols() - 1) || (toRemoveIndex > matrix.getRows() - 1)) {
            throw new IllegalArgumentException("Index to be removed is out of boundary!");
        }
    }

    public void checkIfSquare(Matrix matrix) {
        if (matrix.getRows() != matrix.getCols()) {
            throw new IllegalArgumentException("Not square matrix!");
        }
    }

    public Matrix copySquareMatrix(Matrix matrix){
        return copyBehaviour.copySquareMatrix(matrix);
    }

    private void getCofactor(int[][] matrix, int[][] temp, int q, int dimension) {
        int i = 0, j = 0;
        for (int row = 0; row < dimension; row++) {
            for (int col = 0; col < dimension; col++) {
                if (row != 0 && col != q) {
                    temp[i][j++] = matrix[row][col];
                    if (j == dimension - 1) {
                        j = 0;
                        i++;
                    }
                }
            }
        }
    }

    public int determinant(int[][] matrix, int dimension) {
        int result = 0;
        if (dimension == 1) {
            return matrix[0][0];
        }
        int[][] temp = new int[dimension][dimension];
        int sign = 1;
        for (int i = 0; i < dimension; i++) {
            getCofactor(matrix, temp, i, dimension);
            result += sign * matrix[0][i] * determinant(temp, dimension - 1);
            sign = -sign;
        }
        return result;
    }

    private void checkIfEqualSizes(Matrix leftMatrix, Matrix rightMatrix) {
        if ((leftMatrix.getRows() != leftMatrix.getCols()) || (rightMatrix.getRows() != rightMatrix.getCols())) {
            throw new IllegalArgumentException("Different size matrices!");
        }
    }

    private void checkMatricesSizesForMultiplication(Matrix leftMatrix, Matrix rightMatrix) {
        if (leftMatrix.getCols() != rightMatrix.getRows()) {
            throw new IllegalArgumentException("Invalid Matrices Size For Multiplication!");
        }
    }
}
