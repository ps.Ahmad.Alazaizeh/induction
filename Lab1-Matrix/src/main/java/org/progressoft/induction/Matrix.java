package org.progressoft.induction;

import java.util.Arrays;

public class Matrix {

    private final int rows;
    private final int cols;
    private final int[][] array;

    public Matrix(int[][] array) {
        checkIfRowIsNull(array);
        checkIfValid(array);

        this.rows = array.length;
        this.cols = array[0].length;
        this.array = cloneArray(array);
    }

    private int[][] cloneArray(int[][] array) {
        int[][] tempArray = new int[array.length][];
        for (int i = 0; i < array.length; i++) {
            tempArray[i] = Arrays.copyOf(array[i], array[i].length);
        }
        return tempArray;
    }

    private void checkIfValid(int[][] array) {
        int tempColSize = array[0].length;
        for (int[] ints : array) {
            for (int j = 0; j < ints.length; j++) {
                if (ints.length != tempColSize) {
                    throw new IllegalArgumentException("Invalid Matrix!");
                }
            }
        }
    }

    private void checkIfRowIsNull(int[][] array) {
        for (int[] ints : array) {
            if (ints == null) {
                throw new IllegalArgumentException("Null Row!");
            }
        }
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }

    public int getValue(int i, int j) {
        return array[i][j];
    }

    public int[][] getArray() {
        return array;
    }
}
