package com.progressoft;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.progressoft.induction.*;

public class MatrixUtilityTest {

    //Sum Tests
    @Test
    public void givenInvalidLeftSideMatrix_whenSum_thenThrowException() {
        int[][] leftArray = {
                {1, 2, 3},
                {4, 5},
                {7, 8, 9}
        };
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new Matrix(leftArray));
        Assertions.assertEquals("Invalid Matrix!", exception.getMessage(), "Invalid Left Side Matrix!");
    }

    @Test
    public void givenValidLeftSideMatrixAndInvalidRightSideMatrix_whenSum_thenThrowException() {
        int[][] rightArray = {
                {1, 2, 3},
                {4, 5},
                {7, 8, 9}
        };
        IllegalArgumentException exception1 = Assertions.assertThrows(IllegalArgumentException.class, () -> new Matrix(rightArray));
        Assertions.assertEquals("Invalid Matrix!", exception1.getMessage(), "Invalid Right Side Matrix!");
    }

    @Test
    public void givenValidMatricesWithDifferentSizes_whenSum_thenThrowException() {
        MatrixUtility matrixUtility = new MatrixUtility();
        Matrix leftMatrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        });
        Matrix rightMatrix = new Matrix(new int[][]{
                {9, 8, 7},
                {6, 5, 8},
                {3, 2, 1},
                {7, 3, 8}
        });
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> matrixUtility.sum(leftMatrix, rightMatrix));
        Assertions.assertEquals("Different size matrices!", exception.getMessage());
    }

    @Test
    public void givenValidMatrices_whenSum_thenReturnResult() {
        MatrixUtility matrixUtility = new MatrixUtility();
        Matrix leftMatrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        });
        Matrix rightMatrix = new Matrix(new int[][]{
                {9, 8, 7},
                {6, 5, 4},
                {3, 2, 1},
        });
        Matrix resultMatrix = matrixUtility.sum(leftMatrix, rightMatrix);
        Matrix expectedResultMatrix = new Matrix(new int[][]{
                {10, 10, 10},
                {10, 10, 10},
                {10, 10, 10}
        });
        Assertions.assertArrayEquals(expectedResultMatrix.getArray(), resultMatrix.getArray(), "Wrong Result!");
    }

    //Scalar Multiplication Test
    @Test
    public void givenValidMatrix_whenScalarMultiplication_thenReturnResult() {
        MatrixUtility matrixUtility = new MatrixUtility();
        int multiplier = 2;
        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        });
        Matrix expectedResultMatrix = new Matrix(new int[][]{
                {2, 4, 6},
                {8, 10, 12},
                {14, 16, 18}
        });
        Matrix resultMatrix = matrixUtility.scalarMultiplication(multiplier, matrix);
        Assertions.assertArrayEquals(expectedResultMatrix.getArray(), resultMatrix.getArray(), "Wrong Result!");
    }

    //Transportation Tests
    @Test
    public void givenValidMatrix_whenTransportation_thenReturnResult() {
        MatrixUtility matrixUtility = new MatrixUtility();
        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6}
        });
        Matrix expectedResultMatrix = new Matrix(new int[][]{
                {1, 4},
                {2, 5},
                {3, 6}
        });
        Matrix resultMatrix = matrixUtility.transportation(matrix);
        Assertions.assertArrayEquals(expectedResultMatrix.getArray(), resultMatrix.getArray(), "Wrong Result!");
    }

    //Multiplication Tests
    @Test
    public void givenInvalidLeftSideMatrix_whenMultiplication_thenThrowException() {
        int[][] leftArray = {
                {1, 2, 3},
                {4, 5},
                {7, 8, 9}
        };
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new Matrix(leftArray));
        Assertions.assertEquals("Invalid Matrix!", exception.getMessage(), "Invalid Left Side Matrix!");
    }

    @Test
    public void givenValidLeftSideMatrixAndInvalidRightSideMatrix_whenMultiplication_thenThrowException() {
        int[][] rightArray = {
                {1, 2, 3},
                {4, 5},
                {7, 8, 9}
        };
        IllegalArgumentException exception1 = Assertions.assertThrows(IllegalArgumentException.class, () -> new Matrix(rightArray));
        Assertions.assertEquals("Invalid Matrix!", exception1.getMessage(), "Invalid Right Side Matrix!");
    }

    @Test
    public void givenInvalidMatricesSizesForMultiplication_whenMultiplication_thenThrowException() {
        MatrixUtility matrixUtility = new MatrixUtility();
        Matrix leftMatrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6}
        });
        Matrix rightMatrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6}
        });
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> matrixUtility.multiplication(leftMatrix, rightMatrix));
        Assertions.assertEquals("Invalid Matrices Size For Multiplication!", exception.getMessage());
    }

    @Test
    public void givenValidMatricesWithValidSizes_whenMultiplication_thenReturnResult() {
        MatrixUtility matrixUtility = new MatrixUtility();
        Matrix leftMatrix = new Matrix(new int[][]{
                {1, 2},
                {3, 4},
                {5, 6}
        });
        Matrix rightMatrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6}
        });
        Matrix expectedResultMatrix = new Matrix(new int[][]{
                {9, 12, 15},
                {19, 26, 33},
                {29, 40, 51}
        });
        Matrix resultMatrix = matrixUtility.multiplication(leftMatrix, rightMatrix);
        Assertions.assertArrayEquals(expectedResultMatrix.getArray(), resultMatrix.getArray(), "Wrong Result!");
    }

    //SubMatrix Test
    @Test
    public void givenValidMatrix_whenSubMatrix_thenReturnResult() {
        MatrixUtility matrixUtility = new MatrixUtility();
        int rowToRemoveIndex = 1;
        int colToRemoveIndex = 1;
        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        });
        Matrix expectedMatrix = new Matrix(new int[][]{
                {1, 3},
                {7, 9}
        });
        Matrix resultMatrix = matrixUtility.subMatrix(matrix, rowToRemoveIndex, colToRemoveIndex);
        Assertions.assertArrayEquals(expectedMatrix.getArray(), resultMatrix.getArray(), "Wrong Result!");
    }

    @Test
    public void givenValidMatrix_whenSubMatrixRemoveRowOnly_thenReturnResult() {
        MatrixUtility matrixUtility = new MatrixUtility();
        int toRemoveIndex = 1;
        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        });
        Matrix expectedMatrix = new Matrix(new int[][]{
                {1, 2, 3},
                {7, 8, 9}
        });
        Matrix resultMatrix = matrixUtility.subMatrix(matrix, toRemoveIndex, true);
        Assertions.assertArrayEquals(expectedMatrix.getArray(), resultMatrix.getArray(), "Wrong Result!");
    }

    @Test
    public void givenOutOfBoundaryIndexForRowToRemove_whenSubMatrix_thenThrowException() {
        MatrixUtility matrixUtility = new MatrixUtility();
        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        });
        int rowToRemoveIndex = 3;
        int colToRemoveIndex = 0;
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> matrixUtility.subMatrix(matrix, rowToRemoveIndex, colToRemoveIndex));
        Assertions.assertEquals("Index to be removed is out of boundary!", exception.getMessage());
    }

    @Test
    public void givenInBoundaryIndexForRowAndOutOfBoundaryIndexForColToRemove_whenSubMatrix_thenThrowException() {
        MatrixUtility matrixUtility = new MatrixUtility();
        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        });
        int rowToRemoveIndex = 0;
        int colToRemoveIndex = 3;

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> matrixUtility.subMatrix(matrix, rowToRemoveIndex, colToRemoveIndex));
        Assertions.assertEquals("Index to be removed is out of boundary!", exception.getMessage());
    }

    //Square Operations Tests
    @Test
    public void givenInvalidSquareMatrix_whenSquareOperations_thenThrowException() {
        MatrixUtility matrixUtility = new MatrixUtility();
        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6}
        });

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> matrixUtility.checkIfSquare(matrix));
        Assertions.assertEquals("Not square matrix!", exception.getMessage());
    }

    @Test
    public void givenValidSquareMatrix_whenDiagonalOperation_thenReturnResult() {
        MatrixUtility matrixUtility = new MatrixUtility(new DiagonalCopyBehaviour());
        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        });
        Matrix expectedResultMatrix = new Matrix(new int[][]{
                {1, 0, 0},
                {0, 5, 0},
                {0, 0, 9}
        });
        Matrix resultMatrix = matrixUtility.copySquareMatrix(matrix);
        Assertions.assertArrayEquals(expectedResultMatrix.getArray(), resultMatrix.getArray(), "Wrong result!");
    }

    @Test
    public void givenValidSquareMatrix_whenLowerTriangularOperation_thenReturnResult() {
        MatrixUtility matrixUtility = new MatrixUtility(new LowerTriangularCopyBehaviour());
        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        });
        Matrix expectedResultMatrix = new Matrix(new int[][]{
                {1, 0, 0},
                {4, 5, 0},
                {7, 8, 9}
        });
        Matrix resultMatrix = matrixUtility.copySquareMatrix(matrix);

        Assertions.assertArrayEquals(expectedResultMatrix.getArray(), resultMatrix.getArray(), "Wrong result!");
    }

    @Test
    public void givenValidSquareMatrix_whenUpperTriangularOperation_thenReturnResult() {
        MatrixUtility matrixUtility = new MatrixUtility(new UpperTriangularCopyBehaviour());
        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        });
        Matrix expectedResultMatrix = new Matrix(new int[][]{
                {1, 2, 3},
                {0, 5, 6},
                {0, 0, 9}
        });
        Matrix resultMatrix = matrixUtility.copySquareMatrix(matrix);

        Assertions.assertArrayEquals(expectedResultMatrix.getArray(), resultMatrix.getArray(), "Wrong result!");
    }

    //Determinant Test
    @Test
    public void givenInvalidSquareMatrix_whenDeterminant_thenThrowException() {
        MatrixUtility matrixUtility = new MatrixUtility();
        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 3},
                {4, 5, 6}
        });
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> matrixUtility.checkIfSquare(matrix));
        Assertions.assertEquals("Not square matrix!", exception.getMessage());
    }

    @Test
    public void givenValidSquareMatrix_whenDeterminant_thenReturnResult() {
        MatrixUtility matrixUtility = new MatrixUtility();
        Matrix matrix = new Matrix(new int[][]{
                {1, 2, 1},
                {4, 1, 5},
                {8, 3, 7}
        });
        int dimension = matrix.getRows();
        int expectedResult = 20;
        int result = matrixUtility.determinant(matrix.getArray(), dimension);

        Assertions.assertEquals(expectedResult, result, "Wrong result!");
    }
}