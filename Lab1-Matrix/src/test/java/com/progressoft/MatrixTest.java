package com.progressoft;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.progressoft.induction.Matrix;

public class MatrixTest {

    @Test
    public void givenInvalidMatrix_whenConstructed_thenThrowException() {
        int[][] array = {
                {1, 2, 3},
                {4, 5},
                {7, 8, 9}
        };
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new Matrix(array));
        Assertions.assertEquals("Invalid Matrix!", exception.getMessage());
    }

    @Test
    public void givenNullRow_whenConstructed_thenThrowException() {
        int[][] array = {
                {1, 2, 3},
                null,
                {7, 8, 9}
        };
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new Matrix(array));
        Assertions.assertEquals("Null Row!", exception.getMessage());
    }

    @Test
    public void givenValidMatrix_whenConstructed_thenSuccess(){
        Matrix matrix = new Matrix( new int[][] {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        });
        Assertions.assertArrayEquals(matrix.getArray(), matrix.getArray(), "Wrong Array!");
        Assertions.assertEquals(3, matrix.getRows(), "Wrong number of rows!");
        Assertions.assertEquals(3, matrix.getCols(), "Invalid number of cols!");
        Assertions.assertEquals(5, matrix.getValue(1,1), "Wrong value!");
        Assertions.assertEquals(8, matrix.getValue(2,1), "Wrong value!");
    }
}

