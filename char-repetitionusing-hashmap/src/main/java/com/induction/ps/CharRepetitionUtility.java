package com.induction.ps;

import java.util.LinkedHashMap;
import java.util.Map;

public class CharRepetitionUtility {
    private static final Map<Character, Integer> stringRepetition = new LinkedHashMap<>();

    public static String getRepetition(String string) {
        validateString(string);
        char[] charsArray = removeWhitespaces(string).toCharArray();
        for (char c : charsArray) {
            if (!stringRepetition.containsKey(c)){
                stringRepetition.put(c, 1);
            }
            else {
                stringRepetition.put(c, stringRepetition.get(c) + 1);
            }
        }
        return stringRepetition.toString();
    }

    private static String removeWhitespaces(String string) {
        return string.replaceAll("\\s+", "");
    }

    private static void validateString(String string) {
        if (string == null){
            throw new NullPointerException("Invalid! Null string provided!");
        }
        if (string.equals("")){
            throw new IllegalArgumentException("Invalid! Empty string provided!");
        }
    }
}
