package com.induction.ps;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CharRepetitionUtilityTest {

    @Test
    public void givenNullString_whenGetRepetition_thenThrowException(){
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, ()-> CharRepetitionUtility.getRepetition(null));
        Assertions.assertEquals("Invalid! Null string provided!", exception.getMessage());
    }

    @Test
    public void givenEmptyString_whenGetRepetition_thenThrowException(){
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, ()-> CharRepetitionUtility.getRepetition(""));
        Assertions.assertEquals("Invalid! Empty string provided!", exception.getMessage());
    }

    @Test
    public void givenValidUtility_whenGetRepetition_thenSuccess(){
        String actual = (CharRepetitionUtility.getRepetition("Hello people of the world"));
        String expected = "{H=1, e=4, l=4, o=4, p=2, f=1, t=1, h=1, w=1, r=1, d=1}";
        Assertions.assertEquals(expected, actual, "Wrong result!");
    }
}
