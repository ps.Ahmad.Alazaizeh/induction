package com.induction.ps;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Employee {

    private final String firstName;
    private final String lastName;
    private final Date birthDate;
    private final String birthPlace;
    private final Date hiringDate;
    private final Date resignationDate;
    private final String position;
    private final BigDecimal salary;

    Employee(String firstName, String lastName, String birthDate, String birthPlace, String hiringDate, String resignationDate, String position, BigDecimal salary) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        validateDates(birthDate, hiringDate);
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthPlace = birthPlace;
        this.position = position;
        this.salary = salary;
        this.birthDate = sdf.parse(birthDate);
        this.hiringDate = sdf.parse(hiringDate);
        if (resignationDate != null) {
            this.resignationDate = sdf.parse(resignationDate);
        }
        else {
            this.resignationDate = null;
        }
    }

    private void validateDates(String birthDate, String hiringDate) {
        if (birthDate == null || hiringDate == null){
            throw new NullPointerException("Null date provided!");
        }
        if (birthDate.isEmpty() || hiringDate.isEmpty()){
            throw new IllegalArgumentException("Empty date provided!");
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public String getBirthYear() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(birthDate);
        return ("" + calendar.get(Calendar.YEAR) + "");
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public Date getHiringDate() {
        return hiringDate;
    }

    public Date getResignationDate() {
        return resignationDate;
    }

    public String getResignationDateYear() {
        if (resignationDate == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(resignationDate);
        return ("" + calendar.get(Calendar.YEAR) + "");
    }

    public String getPosition() {
        return position;
    }

    public BigDecimal getSalary() {
        return salary;
    }
}
