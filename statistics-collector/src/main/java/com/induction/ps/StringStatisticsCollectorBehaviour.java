package com.induction.ps;

import java.util.*;

public class StringStatisticsCollectorBehaviour implements StatisticsCollector<String, Statistic>{

    private static final String NUMBER_OF_UPPER_CASES = "Upper case";
    private static final String NUMBER_OF_LOWER_CASES = "Lower case";
    private static final String NUMBER_OF_NON_WORD_CHARACTERS = "Non-words";
    private static final String NUMBER_OF_WORDS = "Words";
    private static final String NUMBER_OF_DIGITS = "Digits";

    private final LinkedList<Statistic> statisticList = new LinkedList<>();
    private Iterable<String> stringList;


    @Override
    public String getName() {
        return "String Statistics";
    }

    @Override
    public Iterable<Statistic> collectStatistics(Iterable<String> stringList) {
        validateStringList(stringList);
        this.stringList = stringList;

        addUpperCases();
        addLowerCases();
        addNonWordChars();
        addWords();
        addNumbers();

        return statisticList;
    }

    private void addNumbers() {
        int count = 0;
        for (String s : stringList) {
            for (int i = 0; i < s.length(); i++) {
                if (Character.isDigit(s.charAt(i))){
                    count++;
                }
            }
        }
        addToStatisticsList(NUMBER_OF_DIGITS, count);
    }

    private void addWords() {
        int count = 0;
        for (String s : stringList) {
            count += s.split("\\s+").length;
        }
        addToStatisticsList(NUMBER_OF_WORDS, count);
    }

    private void addNonWordChars() {
        int count = 0;
        for (String s : stringList) {
            char[] charArray = s.toCharArray();
            for (int i = 0; i < s.length(); i++) {
                if(!(Character.isLetterOrDigit(charArray[i])) && !(Character.isSpaceChar(charArray[i]))){
                    count++;
                }
            }
        }
        addToStatisticsList(NUMBER_OF_NON_WORD_CHARACTERS, count);
    }

    private void addLowerCases() {
        int count = 0;
        for (String s : stringList) {
            char[] charArray = s.toCharArray();
            for (int i = 0; i < s.length(); i++) {
                if(Character.isLowerCase(charArray[i])){
                    count++;
                }
            }
        }
        addToStatisticsList(NUMBER_OF_LOWER_CASES, count);
    }

    private void addUpperCases() {
        int count = 0;
        for (String s : stringList) {
            char[] charArray = s.toCharArray();
            for (int i = 0; i < s.length(); i++) {
                if(Character.isUpperCase(charArray[i])){
                    count++;
                }
            }
        }
        addToStatisticsList(NUMBER_OF_UPPER_CASES, count);
    }

    private void addToStatisticsList(String key, int value) {
        statisticList.add(new StringStatisticsBehaviour(key, value));
    }

    private void validateStringList(Iterable<String> stringList) {
        if (stringList == null){
            throw new NullPointerException("Invalid! Null stringList provided!");
        }
        if (stringList.equals(Collections.emptyList())){
            throw new NullPointerException("Invalid! Empty stringList provided!");
        }
    }
}
