package com.induction.ps;

public class StringStatisticsBehaviour implements Statistic{

    private String key;
    private int value;

    StringStatisticsBehaviour(String key, int value){
        validateKey(key);
        this.key = key;
        this.value = value;
    }

    private void validateKey(String key) {
        if (key == null){
            throw new NullPointerException("Null key provided!");
        }
        if (key.isEmpty()){
            throw new IllegalArgumentException("Empty key provided!");
        }
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Object getValue() {
        return value;
    }

    public void setKey(String key){
        this.key = key;
    }

    public void setValue(int value){
        this.value = value;
    }
}
