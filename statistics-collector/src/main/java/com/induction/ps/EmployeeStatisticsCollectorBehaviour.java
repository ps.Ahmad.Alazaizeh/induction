package com.induction.ps;

import java.util.*;

public class EmployeeStatisticsCollectorBehaviour implements StatisticsCollector<Employee, Statistic> {

    private static final String BORN_IN = "born in ";
    private static final String LESS_THAN_350 = "salary < 350";
    private static final String BETWEEN_350_AND_600 = "350 <= salary < 600";
    private static final String BETWEEN_600_AND_1200 = "600 <= salary < 1,200";
    private static final String MORE_THAN_1200 = "salary >= 1,200";
    private static final String RESIGNED_IN = "resigned in ";

    @Override
    public String getName() {
        return "Employee Statistics";
    }

    @Override
    public Iterable<Statistic> collectStatistics(Iterable<Employee> employeeList) {
        validateEmployeesList(employeeList);
        LinkedList<Statistic> statisticList = new LinkedList<>();

        addBirthYear(employeeList, statisticList);
        addBirthPlace(employeeList, statisticList);
        addSalary(employeeList, statisticList);
        addResignationDate(employeeList, statisticList);
        addPosition(employeeList, statisticList);

        return statisticList;
    }

    private void validateEmployeesList(Iterable<Employee> employeeList) {
        if (employeeList == null){
            throw new NullPointerException("Invalid! Null employeesList provided!");
        }
        if (employeeList.equals(Collections.emptyList())){
            throw new NullPointerException("Invalid! Empty employeesList provided!");
        }
    }

    private void addPosition(Iterable<Employee> employeesList, List<Statistic> statisticList) {
        for (Employee e : employeesList) {
            addToStatisticsList(statisticList, e.getPosition());
        }
    }

    private void addResignationDate(Iterable<Employee> employeesList, List<Statistic> statistic) {
        for (Employee e : employeesList) {
            if (e.getResignationDateYear() == null){
                continue;
            }
            addToStatisticsList(statistic, RESIGNED_IN+e.getResignationDateYear());
        }
    }

    private void addSalary(Iterable<Employee> employeesList, List<Statistic> statistic) {
        for (Employee e : employeesList) {
            if (e.getSalary().intValue() < 350){
                addToStatisticsList(statistic, LESS_THAN_350);
            }
            else if (e.getSalary().intValue() < 600){
                addToStatisticsList(statistic, BETWEEN_350_AND_600);
            }
            else if (e.getSalary().intValue() < 1200){
                addToStatisticsList(statistic, BETWEEN_600_AND_1200);
            }
            else {
                addToStatisticsList(statistic, MORE_THAN_1200);
            }
        }
    }

    private void addBirthPlace(Iterable<Employee> employeesList, List<Statistic> statistic) {
        for (Employee e : employeesList) {
            addToStatisticsList(statistic, BORN_IN+e.getBirthPlace());
        }
    }

    private void addBirthYear(Iterable<Employee> employeesList, List<Statistic> statistic) {
        for (Employee e : employeesList) {
            addToStatisticsList(statistic, BORN_IN+e.getBirthYear());
        }
    }

    private void addToStatisticsList(List<Statistic> statisticList, String key) {

        if (statisticList.stream().noneMatch(o -> o.getKey().equals(key))){
            statisticList.add(new EmployeeStatisticsBehaviour(key,1));
        }
        else {
            EmployeeStatisticsBehaviour ESB = (EmployeeStatisticsBehaviour) statisticList.stream().filter(S-> key.equals(S.getKey()))
                    .findFirst().orElse(null);
            Objects.requireNonNull(ESB).setValue((Integer) ESB.getValue()+1);
        }
    }
}
