package com.induction.ps;

public interface StatisticsCollector<T, S extends Statistic> {
    /**
     * Returns a descriptive name of this collector, i.e: employees information
     * statistics
     *
     * @return collector name
     */
    String getName();
    /**
     * Accept then collect statistics from passed <code>objects</code>.
     * <p>
     *
     * @param objects
     * the objects to collect statistics from
     * @return calculated statistics
     */
    Iterable<S> collectStatistics(Iterable<T> objects);
}
