package com.induction.ps;

public class EmployeeStatisticsBehaviour implements Statistic{

    private String key;
    private int value;

    EmployeeStatisticsBehaviour(String key, int value){
        this.key = key;
        this.value = value;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Object getValue() {
        return value;
    }

    public void setKey(String key){
        this.key = key;
    }

    public void setValue(int value){
        this.value = value;
    }
}
