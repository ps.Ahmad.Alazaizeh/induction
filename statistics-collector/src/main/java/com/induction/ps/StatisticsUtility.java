package com.induction.ps;

public class StatisticsUtility<T, S extends Statistic> {

    private final StatisticsCollector<T, S> statisticsCollector;

    StatisticsUtility(StatisticsCollector<T, S> statisticsCollector){
        this.statisticsCollector = statisticsCollector;
    }
    
    public String getName(){
        return statisticsCollector.getName();
    }
    
    public Iterable<S> collectStatistics(Iterable<T> objects){
        Iterable<S> collectedStatistics = statisticsCollector.collectStatistics(objects);
        printStatistics(collectedStatistics);
        return collectedStatistics;
    }

    private void printStatistics(Iterable<S> collectedStatistics) {
        System.out.println(getName()+":");
        System.out.printf("%-25s| %-30s%n", "Key", "Value");
        for (S s : collectedStatistics) {
            System.out.println("****************************************");
            System.out.printf("%-25s| %-20s%n", s.getKey(), s.getValue());
        }
    }
}
