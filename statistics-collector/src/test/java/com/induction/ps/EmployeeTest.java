package com.induction.ps;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;

public class EmployeeTest {

    @Test
    public void givenValidEmployee_whenConstructed_thenSuccess() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        Employee employee = new Employee("Ahmed", "Alazaizeh", "01-05-1998", "Irbid", "29-05-2022", null, "Java Developer", new BigDecimal(1000));
        Assertions.assertEquals("Ahmed", employee.getFirstName(), "FirstName is not as expected!");
        Assertions.assertEquals("Alazaizeh", employee.getLastName(), "LastName is not as expected!");
        calendar.setTime(employee.getBirthDate());
        Assertions.assertEquals(1998, calendar.get(Calendar.YEAR), "BirthDate is not as expected!");
        Assertions.assertEquals(5, calendar.get(Calendar.MONTH)+1, "BirthDate is not as expected!");
        Assertions.assertEquals(1, calendar.get(Calendar.DAY_OF_MONTH), "BirthDate is not as expected!");
        Assertions.assertEquals("Irbid", employee.getBirthPlace(), "BirthPlace is not as expected!");
        calendar.setTime(employee.getHiringDate());
        Assertions.assertEquals(2022, calendar.get(Calendar.YEAR), "HiringDate is not as expected!");
        Assertions.assertEquals(5, calendar.get(Calendar.MONTH)+1, "HiringDate is not as expected!");
        Assertions.assertEquals(29, calendar.get(Calendar.DAY_OF_MONTH), "HiringDate is not as expected!");
        Assertions.assertNull(employee.getResignationDate(), "ResignationDate is not as expected!");
        Assertions.assertEquals("Java Developer", employee.getPosition(), "Position is not as expected!");
        Assertions.assertEquals("1000", employee.getSalary().toString(), "Salary is not as expected!");
        Assertions.assertEquals("1998", employee.getBirthYear(), "BirthYear is not as expected!");
    }

    @Test
    public void givenNullDate_whenConstructed_thenThrowException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> new Employee("Ahmed", "Alazaizeh", null, "Irbid", "29-05-2022", null, "Java Developer", new BigDecimal(1000)));
        Assertions.assertEquals("Null date provided!", exception.getMessage());
    }

    @Test
    public void givenEmptyDate_whenConstructed_thenThrowException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new Employee("Ahmed", "Alazaizeh", "", "Irbid", "29-05-2022", null, "Java Developer", new BigDecimal(1000)));
        Assertions.assertEquals("Empty date provided!", exception.getMessage());
    }
}
