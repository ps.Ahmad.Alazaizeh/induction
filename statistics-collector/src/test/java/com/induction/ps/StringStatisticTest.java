package com.induction.ps;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StringStatisticTest {

    @Test
    public void givenValidStringStatistic_whenConstructed_thenSuccess() {
        StringStatisticsBehaviour stringStatisticsBehaviour = new StringStatisticsBehaviour("test", 0);
        stringStatisticsBehaviour.setKey("ONE");
        stringStatisticsBehaviour.setValue(1);
        Assertions.assertEquals("ONE", stringStatisticsBehaviour.getKey(), "Key is not as expected");
        Assertions.assertEquals(1, stringStatisticsBehaviour.getValue(), "Value is not as expected");
    }

    @Test
    public void givenNullKey_whenConstructed_thenThrowException(){
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, ()-> new StringStatisticsBehaviour(null, 0));
        Assertions.assertEquals("Null key provided!", exception.getMessage());
    }

    @Test
    public void givenEmptyKey_whenConstructed_thenThrowException(){
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, ()-> new StringStatisticsBehaviour("", 0));
        Assertions.assertEquals("Empty key provided!", exception.getMessage());
    }
}
