package com.induction.ps;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class EmployeeStatisticCollectorTest {

    @Test
    public void givenNullEmployeeList_whenCollectStatistics_thenThrowException(){
        List<Employee> employeesList = null;
        StatisticsUtility<Employee, Statistic> statisticsUtility = new StatisticsUtility<>(new EmployeeStatisticsCollectorBehaviour());
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, ()-> statisticsUtility.collectStatistics(employeesList));
        Assertions.assertEquals("Invalid! Null employeesList provided!", exception.getMessage());
    }

    @Test
    public void givenEmptyEmployeesList_whenCollectStatistics_thenThrowException(){
        List<Employee> employeesList = new LinkedList<>();
        StatisticsUtility<Employee, Statistic> statisticsUtility = new StatisticsUtility<>(new EmployeeStatisticsCollectorBehaviour());
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, ()-> statisticsUtility.collectStatistics(employeesList));
        Assertions.assertEquals("Invalid! Empty employeesList provided!", exception.getMessage());
    }

    @Test
    public void givenEmployees_whenCollectStatistics_thenStatisticsReturnedAsExpected() throws ParseException {
        List<Employee> employeesList = prepareCases();
        StatisticsUtility<Employee, Statistic> statisticsUtility = new StatisticsUtility<>(new EmployeeStatisticsCollectorBehaviour());
        Iterable<? extends Statistic> statistics = statisticsUtility.collectStatistics(employeesList);
        LinkedList<Statistic> expectedList = new LinkedList<>();
        expectedList.add(new EmployeeStatisticsBehaviour("born in 1997", 2));
        expectedList.add(new EmployeeStatisticsBehaviour("born in 1995", 3));
        expectedList.add(new EmployeeStatisticsBehaviour("born in Jordan", 2));
        expectedList.add(new EmployeeStatisticsBehaviour("born in Palestine", 1));
        expectedList.add(new EmployeeStatisticsBehaviour("born in egypt", 2));
        expectedList.add(new EmployeeStatisticsBehaviour("salary < 350", 1));
        expectedList.add(new EmployeeStatisticsBehaviour("350 <= salary < 600", 1));
        expectedList.add(new EmployeeStatisticsBehaviour("600 <= salary < 1,200", 2));
        expectedList.add(new EmployeeStatisticsBehaviour("salary >= 1,200", 1));
        expectedList.add(new EmployeeStatisticsBehaviour("resigned in 2018", 2));
        expectedList.add(new EmployeeStatisticsBehaviour("resigned in 2019", 1));
        expectedList.add(new EmployeeStatisticsBehaviour("developer", 3));
        expectedList.add(new EmployeeStatisticsBehaviour("project manager", 1));
        expectedList.add(new EmployeeStatisticsBehaviour("quality control", 1));
        int count = 0;
        for (Statistic s : statistics) {
            Assertions.assertEquals(expectedList.get(count).getKey(), s.getKey(),"returned statistics are not within expected");
            Assertions.assertEquals(expectedList.get(count).getValue(), s.getValue(),"returned statistics are not within expected");
            count++;
        }
    }

    private List<Employee> prepareCases() throws ParseException {
        return Arrays.asList(
                new Employee("Ahmed", "moh", "01-05-1997", "Jordan", "29-05-2022", null, "developer", new BigDecimal(330)),
                new Employee("Osama", "Azayzeh", "26-01-1995", "Palestine", "01-04-2022", null, "project manager", new BigDecimal(530)),
                new Employee("Mohammed", "Anas", "11-08-1995", "egypt", "14-09-2017", "30-4-2018", "developer", new BigDecimal(700)),
                new Employee("anas", "roud", "06-03-1997", "egypt", "14-09-2017", "23-8-2019", "quality control", new BigDecimal(1300)),
                new Employee("sara", "ali", "17-02-1995", "Jordan", "14-09-2017", "01-02-2018", "developer", new BigDecimal(1100))
        );
    }
}