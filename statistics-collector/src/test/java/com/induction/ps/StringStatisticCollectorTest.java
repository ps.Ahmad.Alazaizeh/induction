package com.induction.ps;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

public class StringStatisticCollectorTest {

    @Test
    public void givenNullStringList_whenCollectStatistics_thenThrowException(){
        List<String> stringList = null;
        StatisticsUtility<String, Statistic> statisticsUtility = new StatisticsUtility<>(new StringStatisticsCollectorBehaviour());
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, ()-> statisticsUtility.collectStatistics(stringList));
        Assertions.assertEquals("Invalid! Null stringList provided!", exception.getMessage());
    }

    @Test
    public void givenEmptyStringList_whenCollectStatistics_thenThrowException(){
        List<String> stringList = new LinkedList<>();
        StatisticsUtility<String, Statistic> statisticsUtility = new StatisticsUtility<>(new StringStatisticsCollectorBehaviour());
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class, ()-> statisticsUtility.collectStatistics(stringList));
        Assertions.assertEquals("Invalid! Empty stringList provided!", exception.getMessage());
    }

    @Test
    public void givenStringCases_whenCollectStatistics_thenStatisticsReturnedAsExpected() {
        List<String> stringList = prepareCases();
        StatisticsUtility<String, Statistic> statisticsUtility = new StatisticsUtility<>(new StringStatisticsCollectorBehaviour());
        Iterable<? extends Statistic> statistics = statisticsUtility.collectStatistics(stringList);
        LinkedList<Statistic> expectedList = new LinkedList<>();
        expectedList.add(new StringStatisticsBehaviour("Upper case", 3));
        expectedList.add(new StringStatisticsBehaviour("Lower case", 37));
        expectedList.add(new StringStatisticsBehaviour("Non-words", 4));
        expectedList.add(new StringStatisticsBehaviour("Words", 8));
        expectedList.add(new StringStatisticsBehaviour("Digits", 3));
        int count = 0;
        for (Statistic s : statistics) {
            Assertions.assertEquals(expectedList.get(count).getKey(), s.getKey(), "returned statistics are not within expected");
            Assertions.assertEquals(expectedList.get(count).getValue(), s.getValue(), "returned statistics are not within expected");
            count++;
        }
    }

    private List<String> prepareCases() {
        return Arrays.asList("hello world",
                "Hakona Matata!",
                "my password P@ssw0rd",
                "23%&kl");
    }
}
