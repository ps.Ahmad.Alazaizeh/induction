package com.induction.ps;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EmployeeStatisticsTest {

    @Test
    public void givenValidEmployeeStatistic_whenConstructed_thenSuccess() {
        EmployeeStatisticsBehaviour employeeStatisticsBehaviour = new EmployeeStatisticsBehaviour("", 0);
        employeeStatisticsBehaviour.setKey("ONE");
        employeeStatisticsBehaviour.setValue(1);
        Assertions.assertEquals("ONE", employeeStatisticsBehaviour.getKey(), "Key is not as expected");
        Assertions.assertEquals(1, employeeStatisticsBehaviour.getValue(), "Value is not as expected");
    }
}
