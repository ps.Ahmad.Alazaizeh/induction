package com.induction.ps;

import java.io.*;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class CSVReaderUtility {

    public BigDecimal read(File file, String frequency, String CURRENCY, String COLLECTION) throws IOException {
        BigDecimal rate;
        try (BufferedReader inputStream = new BufferedReader(new FileReader(file))) {
            String header = inputStream.readLine();
            List<String> headerList = Arrays.asList(header.split(","));
            String line;
            List<String> lineList = null;
            while ((line = inputStream.readLine()) != null) {
                if (line.contains(frequency) && line.contains(CURRENCY) && (line.contains(COLLECTION))) {
                    lineList = Arrays.asList(line.split(","));
                    if (lineList.get(4).equals(CURRENCY)) {
                        break;
                    }
                }
            }
            int latestRateIndex = getLastMonthRateIndex(headerList);
            rate = new BigDecimal(lineList.get(latestRateIndex));
        }
        return rate;
    }

    private int getLastMonthRateIndex(List<String> headerList) {
        int index = -1;
        for (String s : headerList) {
            if (!s.matches(".*[a-zA-Z].*")) {
                index = headerList.indexOf(s);
            }
        }
        return index;
    }
}
