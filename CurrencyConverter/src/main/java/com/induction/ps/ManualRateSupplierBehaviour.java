package com.induction.ps;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class ManualRateSupplierBehaviour implements RateSupplier {
    private static final Map<String, BigDecimal> exchangeRates = new HashMap<>();

    @Override
    public BigDecimal getRate (String code){
        checkIfCodeNotExist(code);
        return exchangeRates.get(code);
    }

    public void updateRate(String code, BigDecimal rate){
        checkIfCodeNotExist(code);
        exchangeRates.put(code, rate);
    }

    public void addRate(String code, BigDecimal rate){
        checkIfCodeExist(code);
        exchangeRates.put(code, rate);
    }

    private void checkIfCodeExist(String code) {
        if (exchangeRates.containsKey(code)){
            throw new IllegalArgumentException("Invalid! Code already exists!");
        }
    }

    private void checkIfCodeNotExist(String code) {
        if (!exchangeRates.containsKey(code)){
            throw new IllegalArgumentException("Invalid! Code does not exist!");
        }
    }
}
