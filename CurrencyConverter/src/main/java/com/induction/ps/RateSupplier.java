package com.induction.ps;

import java.io.IOException;
import java.math.BigDecimal;

public interface RateSupplier {
    BigDecimal getRate(String code);
}
