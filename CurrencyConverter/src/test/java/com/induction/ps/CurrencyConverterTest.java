package com.induction.ps;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;

public class CurrencyConverterTest {
    CurrencyConverter currencyConverter = new CurrencyConverter(new ManualRateSupplierBehaviour());
    static ManualRateSupplierBehaviour manualRateSupplierBehaviour = new ManualRateSupplierBehaviour();

    @BeforeAll
    static void initExchangeRates(){
        manualRateSupplierBehaviour.addRate("USD", new BigDecimal("1.0"));
        manualRateSupplierBehaviour.addRate("ARS", new BigDecimal("120.1617"));
        manualRateSupplierBehaviour.addRate("CAD", new BigDecimal("1.266965"));
        manualRateSupplierBehaviour.addRate("CNY", new BigDecimal("6.664986"));
        manualRateSupplierBehaviour.addRate("EUR", new BigDecimal("0.933445"));
        manualRateSupplierBehaviour.addRate("JOD", new BigDecimal("0.71"));
        manualRateSupplierBehaviour.addRate("KWD", new BigDecimal("0.30565"));
        manualRateSupplierBehaviour.addRate("OMR", new BigDecimal("0.3845"));
        manualRateSupplierBehaviour.addRate("SAR", new BigDecimal("3.75"));
        manualRateSupplierBehaviour.addRate("AUD", new BigDecimal("1.393914"));
    }

    @Test
    public void givenAlreadyExistCode_whenAddCode_thenThrowException(){
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, ()-> manualRateSupplierBehaviour.addRate("USD", new BigDecimal("1.0")));
        Assertions.assertEquals("Invalid! Code already exists!", exception.getMessage());
    }

    @Test
    public void givenDoesNotExistCode_whenUpdateCode_thenThrowException(){
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, ()-> manualRateSupplierBehaviour.updateRate("TEST", new BigDecimal("1.0")));
        Assertions.assertEquals("Invalid! Code does not exist!", exception.getMessage());
    }

    @Test
    public void givenNullFromCurrencyCode_whenConverting_thenThrowException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> currencyConverter.convert(null, "USD", new BigDecimal ("1.0")));
        Assertions.assertEquals("Invalid! From code is null!", exception.getMessage());
    }

    @Test
    public void givenNullToCurrencyCode_whenConverting_thenThrowException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> currencyConverter.convert("USD", null, new BigDecimal ("1.0")));
        Assertions.assertEquals("Invalid! To code is null!", exception.getMessage());
    }

    @Test
    public void givenSameCurrencyCodes_whenConverting_thenThrowException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> currencyConverter.convert("USD", "USD", new BigDecimal ("1.0")));
        Assertions.assertEquals("Invalid! Codes are the same!", exception.getMessage());
    }

    @Test
    public void givenZeroAmount_whenConverting_thenThrowException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> currencyConverter.convert("USD", "ARS", new BigDecimal ("0")));
        Assertions.assertEquals("Invalid! Amount is zero!", exception.getMessage());
    }

    @Test
    public void givenNegativeAmount_whenConverting_thenThrowException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> currencyConverter.convert("USD", "ARS", new BigDecimal ("-1.0")));
        Assertions.assertEquals("Invalid! Amount is negative!", exception.getMessage());
    }

    @Test
    public void givenValidCurrencyCodesAndValidAmount_whenUpdateRate_thenSuccessResult(){
        manualRateSupplierBehaviour.addRate("TEST", new BigDecimal("1"));
        manualRateSupplierBehaviour.updateRate("TEST", new BigDecimal("2"));
        Assertions.assertEquals(2, manualRateSupplierBehaviour.getRate("TEST").doubleValue());
    }

    @Test
    public void givenValidCurrencyCodesAndValidAmount_whenConverting_thenSuccessResult() throws IOException {
        BigDecimal actualResult = currencyConverter.convert("USD", "JOD", new BigDecimal("45"));
        Assertions.assertEquals(31.95, actualResult.doubleValue(), "Invalid! Conversion result is wrong! ");

        actualResult = currencyConverter.convert("ARS", "AUD", new BigDecimal("7000.0"));
        Assertions.assertEquals(81.1954905, actualResult.doubleValue(), "Invalid! Conversion result is wrong! ");

        actualResult = currencyConverter.convert("OMR", "CAD", new BigDecimal("220.0"));
        Assertions.assertEquals(724.91936405, actualResult.doubleValue(), "Invalid! Conversion result is wrong! ");

        actualResult = currencyConverter.convert("KWD", "EUR", new BigDecimal ("75.0"));
        Assertions.assertEquals(229.0487341, actualResult.doubleValue(), "Invalid! Conversion result is wrong! ");

        actualResult = currencyConverter.convert("SAR", "USD", new BigDecimal ("1250.0"));
        Assertions.assertEquals(333.33, actualResult.doubleValue(), "Invalid! Conversion result is wrong! ");

        actualResult = currencyConverter.convert("CNY", "CAD", new BigDecimal ("3000.0"));
        Assertions.assertEquals(570.27361615, actualResult.doubleValue(), "Invalid! Conversion result is wrong! ");
    }

    @Test
    public void givenValidCurrencyCodesAndValidAmountUsingCSV_whenConverting_thenSuccessResult() throws IOException {
        CurrencyConverter currencyConverter = new CurrencyConverter(new CSVRateSupplierBehaviour("/home/ahmad/git/induction/CurrencyConverter/src/main/", "WS_XRU_csv_col.csv", "Monthly", "End of period"));

        BigDecimal actualResult = currencyConverter.convert("USD", "JOD", new BigDecimal("45"));
        Assertions.assertEquals(31.95, actualResult.doubleValue(), "Invalid! Conversion result is wrong! ");

        actualResult = currencyConverter.convert("ARS", "AUD", new BigDecimal("7000.0"));
        Assertions.assertEquals(81.1954905, actualResult.doubleValue(), "Invalid! Conversion result is wrong! ");

        actualResult = currencyConverter.convert("OMR", "CAD", new BigDecimal("220.0"));
        Assertions.assertEquals(724.91936405, actualResult.doubleValue(), "Invalid! Conversion result is wrong! ");

        actualResult = currencyConverter.convert("KWD", "EUR", new BigDecimal ("75.0"));
        Assertions.assertEquals(229.0487341, actualResult.doubleValue(), "Invalid! Conversion result is wrong! ");

        actualResult = currencyConverter.convert("SAR", "USD", new BigDecimal ("1250.0"));
        Assertions.assertEquals(333.33, actualResult.doubleValue(), "Invalid! Conversion result is wrong! ");

        actualResult = currencyConverter.convert("CNY", "CAD", new BigDecimal ("3000.0"));
        Assertions.assertEquals(570.27361615, actualResult.doubleValue(), "Invalid! Conversion result is wrong! ");
    }
}
