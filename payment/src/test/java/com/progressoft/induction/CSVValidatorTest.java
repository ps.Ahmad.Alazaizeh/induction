package com.progressoft.induction;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class CSVValidatorTest {

    @Test
    public void givenValidValidator_whenConstructed_thenSuccess() throws SQLException {
        String path = "/home/ahmad/git/induction/payment/src/main/";
        String fileName = "Payments.csv";

        Validator validator = new Validator();
        List<String> payments = new LinkedList<>();
        //valid case
        payments.add("1,u56273811528,AhmedAhmed,u7712693753,HalaHalaaa,1450,JOD,cash,normal");
        //invalid case, required validation
        payments.add("u91625384632,MohamedMohamed,u5986283578,LinaLinaaa,200,USD,cash,normal");
        //invalid case, min length validation
        payments.add("3,u0616,SarahSarah,u2563453234,AbedAbeddd,374,USD,credit_card,vip");
        //invalid case, uniqueness validation
        payments.add("1,u8155293640,NoorNoorrr,u9019925173,NawrasNawras,1990,JOD,credit_card,normal");
        //invalid case, sign validation
        payments.add("5,u6110273549,BilalBilall,u3883466283,DanialDanial,-466,OMR,mobile,normal");
        //invalid case, payment method validation
        payments.add("6,u0162734926,LeenLeennn,u7726385643,BaraBaraaa,852,CAD,dsfhjs,vip");
        //invalid case, urgency validation
        payments.add("7,u5118203648,LamaLamaaa,u9925482684,YasmeenYasmeen,5284,AUD,mobile,ertetve");
        //invalid case, currency validation
        payments.add("8,u2376529265,FarahFarah,u7725392542,SalmaSalma,488,CCC,cash,normal");
        payments.add("9,u0916289368,OsamaOsama,u9925193547,IsmailIsmaill,811,USD,credit_card,normal");
        payments.add("10,u1682329326,YmanYmannn,u2377729518,AnasAnasss,2033,SAR,cash,normal");

//        Assertions.assertNotNull(validator.validate(payments.get(0), 0));
//        Assertions.assertNull(validator.validate(payments.get(1), 0));
//        Assertions.assertNull(validator.validate(payments.get(2), 0));
//        Assertions.assertNull(validator.validate(payments.get(3), 0));
//        Assertions.assertNull(validator.validate(payments.get(4), 0));
//        Assertions.assertNull(validator.validate(payments.get(5), 0));
//        Assertions.assertNull(validator.validate(payments.get(6), 0));
//        Assertions.assertNull(validator.validate(payments.get(7), 0));
//        Assertions.assertNotNull(validator.validate(payments.get(8), 0));
//        Assertions.assertNotNull(validator.validate(payments.get(9), 0));

    }
}
