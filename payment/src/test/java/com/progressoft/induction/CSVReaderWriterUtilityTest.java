package com.progressoft.induction;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

public class CSVReaderWriterUtilityTest {

    @Test
    public void givenValidPaymentSupplier_whenGetPayments_thenSuccess() throws IOException, SQLException {
        String path = "/home/ahmad/git/induction/payment/src/main/";
        String fileName = "Payments.csv";
        PaymentSupplier ps = new CSVPaymentSupplierBehaviour(path, fileName);

        List<Payment> actualList = ps.getPayments();
        List<Payment> expectedList = new LinkedList<>();
        expectedList.add(new Payment("1,u56273811528,AhmedAhmed,u7712693753,HalaHalaaa,1450,JOD,cash,normal"));
        expectedList.add(new Payment("2,u91625384632,MohamedMohamed,u5986283578,LinaLinaaa,200,USD,cash,normal"));
        expectedList.add(new Payment("3,u0616629631,SarahSarah,u2563453234,AbedAbeddd,374,USD,credit_card,vip"));
        expectedList.add(new Payment("4,u8155293640,NoorNoorrr,u9019925173,NawrasNawras,1990,JOD,credit_card,normal"));
        expectedList.add(new Payment("5,u6110273549,BilalBilall,u3883466283,DanialDanial,466,OMR,mobile,normal"));
        expectedList.add(new Payment("6,u0162734926,LeenLeennn,u7726385643,BaraBaraaa,852,CAD,cash,vip"));
        expectedList.add(new Payment("7,u5118203648,LamaLamaaa,u9925482684,YasmeenYasmeen,5284,AUD,mobile,vip"));
        expectedList.add(new Payment("8,u2376529265,FarahFarah,u7725392542,SalmaSalma,488,CAD,cash,normal"));
        expectedList.add(new Payment("9,u0916289368,OsamaOsama,u9925193547,IsmailIsmaill,811,USD,credit_card,normal"));
        expectedList.add(new Payment("10,u1682329326,YmanYmannn,u2377729518,AnasAnasss,2033,SAR,cash,normal"));

        for (int i = 0; i < expectedList.size(); i++) {
            Assertions.assertEquals(expectedList.get(i).getPaymentNumber(), actualList.get(i).getPaymentNumber());
            Assertions.assertEquals(expectedList.get(i).getSenderAccount(), actualList.get(i).getSenderAccount());
            Assertions.assertEquals(expectedList.get(i).getSenderName(), actualList.get(i).getSenderName());
            Assertions.assertEquals(expectedList.get(i).getReceiverAccount(), actualList.get(i).getReceiverAccount());
            Assertions.assertEquals(expectedList.get(i).getReceiverName(), actualList.get(i).getReceiverName());
            Assertions.assertEquals(expectedList.get(i).getAmount(), actualList.get(i).getAmount());
            Assertions.assertEquals(expectedList.get(i).getCurrency(), actualList.get(i).getCurrency());
            Assertions.assertEquals(expectedList.get(i).getPaymentMethod(), actualList.get(i).getPaymentMethod());
            Assertions.assertEquals(expectedList.get(i).getUrgency(), actualList.get(i).getUrgency());
        }
    }

    @Test
    public void givenNonExistingFile_whenRead_thenFail() {
        CSVReaderWriterUtility csvReaderWriterUtility = new CSVReaderWriterUtility();
        File file = new File("");
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> csvReaderWriterUtility.read(file));
        Assertions.assertEquals("File does not exist!", exception.getMessage());
    }

    @Test
    public void givenNonExistingFile_whenWrite_thenFail() {
        CSVReaderWriterUtility csvReaderWriterUtility = new CSVReaderWriterUtility();
        File file = new File("");
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> csvReaderWriterUtility.write(file, file, new HashMap<>()));
        Assertions.assertEquals("File does not exist!", exception.getMessage());
    }

    @Test
    public void givenNullErrorsMap_whenWrite_thenFail() {
        CSVReaderWriterUtility csvReaderWriterUtility = new CSVReaderWriterUtility();
        File file = new File("");
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> csvReaderWriterUtility.write(file, file, null));
        Assertions.assertEquals("Invalid! provided errorsMap is Null", exception.getMessage());
    }

}
