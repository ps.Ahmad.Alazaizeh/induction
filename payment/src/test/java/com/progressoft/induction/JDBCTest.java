package com.progressoft.induction;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class JDBCTest {

    JDBC jdbc = new JDBC("Bank", "root", "P@ssw0rd");

    @Test
    public void givenInvalidLine_whenInsert_thenEmptyTable() throws SQLException {
        jdbc.clearTable("Test");
        List<Payment> paymentList = new LinkedList<>();
        paymentList.add(new Payment("1,u56273811528,AhmedAhmed,u7712693753,HalaHalaaa,1450,JOD,cash,normal"));
        jdbc.insert("Test", paymentList);

        //Invalid line
        paymentList.add(new Payment("5,u6110273549,Bara,u3883466283,DanialDanial,466,OMR,mobile,normal"));
        jdbc.insert("Test", paymentList);

        Assertions.assertTrue(jdbc.checkIfExists("payment_Number", "Test", "1"));
        Assertions.assertTrue(jdbc.checkIfExists("sender_account", "Test", "u56273811528"));
        Assertions.assertTrue(jdbc.checkIfExists("sender_name", "Test", "AhmedAhmed"));
        Assertions.assertTrue(jdbc.checkIfExists("receiver_account", "Test", "u7712693753"));
        Assertions.assertTrue(jdbc.checkIfExists("receiver_name", "Test", "HalaHalaaa"));
        Assertions.assertTrue(jdbc.checkIfExists("amount", "Test", "1450"));
        Assertions.assertTrue(jdbc.checkIfExists("currency", "Test", "JOD"));
        Assertions.assertTrue(jdbc.checkIfExists("payment_method", "Test", "cash"));
        Assertions.assertTrue(jdbc.checkIfExists("urgency", "Test", "normal"));
    }

    @Test
    public void givenValidLine_whenInsert_thenSuccess() throws SQLException {
        jdbc.clearTable("Test");
        List<Payment> paymentList = new LinkedList<>();
        paymentList.add(new Payment("1,u56273811528,AhmedAhmed,u7712693753,HalaHalaaa,1450,JOD,cash,normal"));
        jdbc.insert("Test", paymentList);

        Assertions.assertTrue(jdbc.checkIfExists("payment_Number", "Test", "1"));
        Assertions.assertTrue(jdbc.checkIfExists("sender_account", "Test", "u56273811528"));
        Assertions.assertTrue(jdbc.checkIfExists("sender_name", "Test", "AhmedAhmed"));
        Assertions.assertTrue(jdbc.checkIfExists("receiver_account", "Test", "u7712693753"));
        Assertions.assertTrue(jdbc.checkIfExists("receiver_name", "Test", "HalaHalaaa"));
        Assertions.assertTrue(jdbc.checkIfExists("amount", "Test", "1450"));
        Assertions.assertTrue(jdbc.checkIfExists("currency", "Test", "JOD"));
        Assertions.assertTrue(jdbc.checkIfExists("payment_method", "Test", "cash"));
        Assertions.assertTrue(jdbc.checkIfExists("urgency", "Test", "normal"));

        paymentList.add(new Payment("5,u6110273549,BaraBaraaa,u3883466283,DanialDanial,466,OMR,mobile,normal"));
        jdbc.insert("Test", paymentList);

        Assertions.assertTrue(jdbc.checkIfExists("payment_Number", "Test", "5"));
        Assertions.assertTrue(jdbc.checkIfExists("sender_account", "Test", "u6110273549"));
        Assertions.assertTrue(jdbc.checkIfExists("sender_name", "Test", "BaraBaraaa"));
        Assertions.assertTrue(jdbc.checkIfExists("receiver_account", "Test", "u3883466283"));
        Assertions.assertTrue(jdbc.checkIfExists("receiver_name", "Test", "DanialDanial"));
        Assertions.assertTrue(jdbc.checkIfExists("amount", "Test", "466"));
        Assertions.assertTrue(jdbc.checkIfExists("currency", "Test", "OMR"));
        Assertions.assertTrue(jdbc.checkIfExists("payment_method", "Test", "mobile"));
        Assertions.assertTrue(jdbc.checkIfExists("urgency", "Test", "normal"));
    }

}
