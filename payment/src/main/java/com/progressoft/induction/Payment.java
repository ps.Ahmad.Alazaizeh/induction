package com.progressoft.induction;

public class Payment {
    private final String paymentNumber;
    private final String senderAccount;
    private final String senderName;
    private final String receiverAccount;
    private final String receiverName;
    private final String amount;
    private final String currency;
    private final String paymentMethod;
    private final String urgency;

    public Payment(String paymentLine) {
        String[] lineArray = paymentLine.split(",");
        this.paymentNumber = lineArray[0];
        this.senderAccount = lineArray[1];
        this.senderName = lineArray[2];
        this.receiverAccount = lineArray[3];
        this.receiverName = lineArray[4];
        this.amount = lineArray[5];
        this.currency = lineArray[6];
        this.paymentMethod = lineArray[7];
        this.urgency = lineArray[8];
    }

    public String getPaymentNumber() {
        return paymentNumber;
    }

    public String getSenderAccount() {
        return senderAccount;
    }

    public String getSenderName() {
        return senderName;
    }

    public String getReceiverAccount() {
        return receiverAccount;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public String getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public String getUrgency() {
        return urgency;
    }
}
