package com.progressoft.induction;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class CSVPaymentSupplierBehaviour implements PaymentSupplier {
    private final File file;
    private final File newFile;

    CSVReaderWriterUtility readerWriterUtility = new CSVReaderWriterUtility();

    CSVPaymentSupplierBehaviour(String pathString, String fileName) {
        Path path = Path.of(pathString);
        this.file = path.resolve(fileName).toFile();
        this.newFile = path.resolve(fileName.replace(".csv", "-updated.csv")).toFile();
    }

    @Override
    public List<Payment> getPayments() throws SQLException, IOException {
        return readerWriterUtility.read(file);
    }

    @Override
    public Map<Integer, String> getErrorsMap() {
        return readerWriterUtility.getErrorsMap();
    }

    @Override
    public void writeErrors(Map<Integer, String> errorsMap) {
        readerWriterUtility.write(file, newFile, errorsMap);
    }

}
