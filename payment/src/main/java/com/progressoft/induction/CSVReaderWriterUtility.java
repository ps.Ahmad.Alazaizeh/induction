package com.progressoft.induction;

import java.io.*;
import java.sql.SQLException;
import java.util.*;

public class CSVReaderWriterUtility {

    Map<Integer, String> errorsMap;

    public List<Payment> read(File file) throws SQLException {
        errorsMap = new LinkedHashMap<>();
        Validator validator = new Validator();
        List<Payment> payments = new LinkedList<>();
        try (BufferedReader inputStream = new BufferedReader(new FileReader(file))) {
            inputStream.readLine();
            String line, validLine;
            int index = -1;
            while ((line = inputStream.readLine()) != null) {
                index++;
                validLine = validator.validate(line, index, errorsMap);
                if (validLine != null)
                    payments.add(new Payment(line));
            }
        } catch (IOException e) {
            throw new IllegalArgumentException("File does not exist!");
        }
        return payments;
    }

    public void write(File oldFile, File updatedFile, Map<Integer, String> errorsMap) {
        validateErrorsMap(errorsMap);
        try (BufferedReader inputStream = new BufferedReader(new FileReader(oldFile))) {
            String[] headerLine = getHeaderLine(inputStream);
            int errorIndex = getHeaderLength(headerLine);
            List<String> lineList = getLineAsList(headerLine);
            try (FileWriter writer = new FileWriter(updatedFile)) {
                addHeaderLine(errorsMap, errorIndex, lineList, writer);
                addLines(errorsMap, errorIndex, writer, inputStream);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException("File does not exist!");
        }
    }


    public Map<Integer, String> getErrorsMap() {
        return errorsMap;
    }

    private List<String> getLineAsList(String[] headerLine) {
        return new ArrayList<>(Arrays.asList(headerLine));
    }

    private int getHeaderLength(String[] headerLine) {
        return headerLine.length;
    }

    private String[] getHeaderLine(BufferedReader inputStream) throws IOException {
        return inputStream.readLine().split(",");
    }

    private void addLines(Map<Integer, String> errorsMap, int errorIndex, FileWriter writer, BufferedReader inputStream) throws IOException {
        List<String> lineList;
        String line;
        int i = 0;
        for (line = inputStream.readLine(); line != null; line = inputStream.readLine(), i++) {
            lineList = getLineAsList(line.split(","));
            if (errorsMap != null && errorsMap.containsKey(i)) {
                String errorString = errorsMap.get(i).replace(",", " | ");
                lineList.add(errorIndex, errorString.substring(1, errorString.length() - 1));
            }
            line = String.join(",", lineList);
            writer.append(line).append("\n");
        }
    }

    private void addHeaderLine(Map<Integer, String> errorsMap, int errorIndex, List<String> lineList, FileWriter writer) throws IOException {
        if (errorsMap != null && !errorsMap.isEmpty()) {
            lineList.add(errorIndex, "errors");
        }
        String line = String.join(",", lineList);
        writer.append(line).append("\n");
    }

    private void validateErrorsMap(Map<Integer, String> errorsMap) {
        if (errorsMap == null) {
            throw new IllegalArgumentException("Invalid! provided errorsMap is Null");
        }
    }
}
