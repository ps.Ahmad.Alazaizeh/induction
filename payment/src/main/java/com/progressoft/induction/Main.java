package com.progressoft.induction;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws SQLException, IOException {

        String pathString = "/home/ahmad/git/induction/payment/src/main/";
        String fileName = "Payments.csv";
        String table = "payments";

        PaymentSupplier ps = new CSVPaymentSupplierBehaviour(pathString, fileName);

        JDBC jdbc = new JDBC("Bank", "root", "P@ssw0rd");

        List<Payment> validPaymentsList = ps.getPayments();
        Map<Integer, String> errorsMap = ps.getErrorsMap();
        ps.writeErrors(errorsMap);
        jdbc.insert(table, validPaymentsList);
    }

}
