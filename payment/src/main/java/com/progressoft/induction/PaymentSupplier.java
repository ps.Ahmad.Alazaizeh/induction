package com.progressoft.induction;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface PaymentSupplier {
    List<Payment> getPayments() throws IOException, SQLException;

    Map<Integer, String> getErrorsMap();

    void writeErrors(Map<Integer, String> errorsMap);
}
