package com.progressoft.induction;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Validator {

    private final List<String> errorsList = new LinkedList<>();

    enum paymentMethods {cash, credit_card, mobile}

    enum urgencyTypes {normal, vip}

    List<String> paymentNumberList = new LinkedList<>();

    public String validate(String line, int index, Map<Integer, String> errorsMap) throws SQLException {
        errorsList.clear();
        String[] lineArray = line.split(",");
        if (!validateRequired(lineArray.length)) {
            return null;
        }
        validateUniqueness(lineArray[0]);
        for (int i = 1; i < lineArray.length; i++) {
            if (i < 5) validateMinLength(lineArray[i]);
            if (i == 5) validateSign(lineArray[5]);
            if (i == 6) validateCurrency(lineArray[6]);
            if (i == 7) validatePaymentMethod(lineArray[7]);
            if (i == 8) validateUrgency(lineArray[8]);
        }
        if (errorsList.isEmpty()) {
            return line;
        } else {
            addToErrorsMap(errorsList, index, errorsMap);
            return null;
        }
    }

    private void addToErrorsMap(List<String> errorsList, int index, Map<Integer, String> errorsMap) {
        errorsMap.put(index, errorsList.toString());
    }

    private void validateUniqueness(String value) {
        if (paymentNumberList.contains(value)) {
            errorsList.add("Not unique value of " + value);
        } else {
            paymentNumberList.add(value);
        }
    }

    private void validateMinLength(String value) {
        if (value.length() < 10) {
            errorsList.add("Provided value of " + value + " is too short.");
        }
    }

    private void validateSign(String value) {
        if (Integer.parseInt(value) < 0) {
            errorsList.add("Provided value of " + value + " is negative.");
        }
    }

    private boolean validateRequired(int length) {
        return length == 9;
    }

    private void validateCurrency(String value) throws SQLException {
        JDBC jdbc = new JDBC("Bank", "root", "P@ssw0rd");
        if (!jdbc.checkIfExists("currency_code", "currency", value)) {
            errorsList.add("Provided currency " + value + " is not a valid currency.");
        }
    }

    private void validatePaymentMethod(String value) {
        try {
            paymentMethods pm = paymentMethods.valueOf(value);
        } catch (IllegalArgumentException e) {
            errorsList.add("Provided currency " + value + " is not a valid payment method.");
        }
    }

    private void validateUrgency(String value) {
        try {
            urgencyTypes ut = urgencyTypes.valueOf(value);
        } catch (IllegalArgumentException e) {
            errorsList.add("Provided urgency " + value + " is not a valid urgency.");
        }
    }
}

