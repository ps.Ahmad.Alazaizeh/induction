package com.progressoft.induction;

import java.sql.*;
import java.util.List;

public class JDBC {

    private final String url;
    private final String username;
    private final String password;

    public JDBC(String database, String username, String password) {
        this.url = "jdbc:mysql://localhost:3306/" + database + "?serverTimezone=UTC";
        this.username = username;
        this.password = password;
    }

    public void insert(String table, List<Payment> paymentsList) throws SQLException {
        for (Payment p : paymentsList) {
            try (Connection connection = DriverManager.getConnection(url, username, password)) {
                try (Statement statement = connection.createStatement()) {
                    String query = "INSERT INTO " + table + " ( payment_Number, sender_account, sender_name, receiver_account, receiver_name, amount, currency, payment_method, urgency ) SELECT " + p.getPaymentNumber() + ", '" + p.getSenderAccount() + "', '" + p.getSenderName() + "', '" + p.getReceiverAccount() + "', '" + p.getReceiverName() + "', " + p.getAmount() + ", '" + p.getCurrency() + "', '" + p.getPaymentMethod() + "', '" + p.getUrgency() + "' WHERE NOT EXISTS (SELECT payment_Number FROM " + table + " WHERE payment_Number = '" + p.getPaymentNumber() + "');";
                    statement.execute(query);
                }
            }
        }
    }

    public void clearTable(String table) throws SQLException {
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            try (Statement statement = connection.createStatement()) {
                String query = "DELETE FROM " + table + ";";
                statement.execute(query);
            }
        }
    }

    public Boolean checkIfExists(String column, String table, String value) throws SQLException {
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            try (Statement statement = connection.createStatement()) {
                String query = "SELECT " + column + " FROM " + table + " WHERE " + column + " = '" + value + "';";
                return (statement.executeQuery(query).isBeforeFirst());
            }
        }
    }
}
