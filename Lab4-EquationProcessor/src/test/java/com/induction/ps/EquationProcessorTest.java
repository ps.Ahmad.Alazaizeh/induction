package com.induction.ps;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EquationProcessorTest {
    @Test
    public void givenEmptyInput_whenConstructed_thenThrowException(){
       IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, ()-> new EquationProcessor(""));
       Assertions.assertEquals("Invalid! Empty input!", exception.getMessage());
    }

    @Test
    public void givenInvalidInput_whenConstructed_thenThrowException(){
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, ()-> new EquationProcessor("9*"));
        Assertions.assertEquals("Invalid equation!", exception.getMessage());
        exception = Assertions.assertThrows(IllegalArgumentException.class, ()-> new EquationProcessor("*5+4"));
        Assertions.assertEquals("Invalid equation!", exception.getMessage());
        exception = Assertions.assertThrows(IllegalArgumentException.class, ()-> new EquationProcessor("4.*"));
        Assertions.assertEquals("Invalid equation!", exception.getMessage());
        exception = Assertions.assertThrows(IllegalArgumentException.class, ()-> new EquationProcessor("5+/4"));
        Assertions.assertEquals("Invalid equation!", exception.getMessage());
        exception = Assertions.assertThrows(IllegalArgumentException.class, ()-> new EquationProcessor("5+4-"));
        Assertions.assertEquals("Invalid equation!", exception.getMessage());
        exception = Assertions.assertThrows(IllegalArgumentException.class, ()-> new EquationProcessor("5+4."));
        Assertions.assertEquals("Invalid equation!", exception.getMessage());
        exception = Assertions.assertThrows(IllegalArgumentException.class, ()-> new EquationProcessor("N+4."));
        Assertions.assertEquals("Invalid equation!", exception.getMessage());
        exception = Assertions.assertThrows(IllegalArgumentException.class, ()-> new EquationProcessor("N+4.*3+2"));
        Assertions.assertEquals("Invalid equation!", exception.getMessage());
    }

    @Test
    public void givenValidInputCheckResult_whenCalculate_thenSuccess(){
        EquationProcessor EP = new EquationProcessor("5*-5+4.3+4-5*5.2/4+2-4/1/4*2+4*2-8+2/2+4*5-6");
        Assertions.assertEquals(-8.2, EP.calculate(), "Wrong result!");
        EP = new EquationProcessor("6+3/5*-2.2+4.7/-2+4-6");
        Assertions.assertEquals(0.33000000000000007, EP.calculate(), "Wrong result!");
        EP = new EquationProcessor("7+3*3.3-7/4+55-6/2*-4.5");
        Assertions.assertEquals(83.65, EP.calculate(), "Wrong result!");
        EP = new EquationProcessor("7*-2.4+6+2-5/6+3*22-4.4*2.2");
        Assertions.assertEquals(46.68666666666667, EP.calculate(), "Wrong result!");
        EP = new EquationProcessor("9-4*2+1*5+3-6*3+5-8-9");
        Assertions.assertEquals(-21, EP.calculate(), "Wrong result!");
        EP = new EquationProcessor("7.2*2+5-3/2+4*-3+2-5+8*2");
        Assertions.assertEquals(18.9, EP.calculate(), "Wrong result!");
        EP = new EquationProcessor("7*-3+1-7*4.2+7.1-6");
        Assertions.assertEquals(-48.300000000000004, EP.calculate(), "Wrong result!");
        EP = new EquationProcessor("8/2");
        Assertions.assertEquals(4, EP.calculate(), "Wrong result!");
        EP = new EquationProcessor("7*-3+1-7*4.2+7.1-6/4*2-5+3/-3*2.2+5/5-2*3/6-6+3.4/5.1");
        Assertions.assertEquals(-57.83333333333334, EP.calculate(), "Wrong result!");
    }
}
