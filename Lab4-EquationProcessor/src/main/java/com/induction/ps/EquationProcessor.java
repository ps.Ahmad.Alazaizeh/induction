package com.induction.ps;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;

public class EquationProcessor {
    private Stack<String> operands = new Stack<>();
    private Stack<String> operations = new Stack<>();
    private final String equation;

    public EquationProcessor(String input) {
        checkIfEmpty(input);
        checkIfValid(input);
        this.equation = input;
    }

    public Double calculate() {
        String tempEquation = equation;
        String[] tempEquationArray;
        Map<String, String> variables = new HashMap<>();
        while (true) {
            tempEquationArray = tempEquation.split("(?=[-+/*])", 2);
            operands.push(tempEquationArray[0]);
            if (tempEquationArray.length < 2) {
                break;
            }
            tempEquationArray = tempEquationArray[1].split("(?=(-?\\d)?([a-zA-Z])?)", 2);
            if (tempEquationArray[0].matches("[-+/*]") && !operations.isEmpty()) {
                if (operations.peek().matches("[*/]")) {
                    String right = operands.pop();
                    right = checkIfVariable(variables, right);
                    String left = operands.pop();
                    left = checkIfVariable(variables, left);
                    String op = operations.pop();
                    if (op.equals("*")) {
                        operands.push(String.valueOf(Double.parseDouble(left) * Double.parseDouble(right)));
                    } else {
                        operands.push(String.valueOf(Double.parseDouble(left) / Double.parseDouble(right)));
                    }
                }
            }
            operations.push(tempEquationArray[0]);
            tempEquation = tempEquationArray[1];
        }
        boolean breakFlag = false;
        while (!operations.isEmpty()) {
            if (!operations.contains("*") && !operations.contains("/") && !breakFlag) {
                operands = reverseStack(operands);
                operations = reverseStack(operations);
                breakFlag = true;
            }
            String right = operands.pop();
            right = checkIfVariable(variables, right);
            String left = operands.pop();
            left = checkIfVariable(variables, left);
            String op = operations.pop();
            switch (op) {
                case "-" -> operands.push(String.valueOf(Double.parseDouble(right) - Double.parseDouble(left)));
                case "+" -> operands.push(String.valueOf(Double.parseDouble(right) + Double.parseDouble(left)));
                case "/" -> operands.push(String.valueOf(Double.parseDouble(left) / Double.parseDouble(right)));
                case "*" -> operands.push(String.valueOf(Double.parseDouble(right) * Double.parseDouble(left)));
            }
        }
        return Double.parseDouble(operands.get(0));
    }

    private String checkIfVariable(Map<String, String> variables, String op) {
        if (op.matches("[a-zA-Z]")){
            op = getVariableValue(variables, op);
        }
        return op;
    }

    private String getVariableValue(Map<String, String> variables, String op) {
        if (variables.containsKey(op)){
            return variables.get(op);
        }
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter value for ("+ op +"): ");
        variables.put(op, scanner.nextLine());
        return variables.get(op);
    }

    private Stack<String> reverseStack(Stack<String> stack) {
        Stack<String> tempOperations = new Stack<>();
        while (!stack.isEmpty()){
            tempOperations.push(stack.pop());
        }
        return tempOperations;
    }

    private void checkIfValid(String input) {
        String regex = "^(-?(\\d+(\\.\\d+)?)?([a-zA-Z])?([-+/*](?![+/*]))?)*";
        if (!((input.matches(regex)) && (input.substring(0, 1).matches("(-)?(\\d)?([a-zA-Z])?")) && (!(input.substring(input.length() - 1)).matches("[-+*/]")) && !(input.substring(0, 1).matches("(-)([a-zA-Z])")))) {
            throw new IllegalArgumentException("Invalid equation!");
        }
    }

    private void checkIfEmpty(String input) {
        if (input.equals("")) {
            throw new IllegalArgumentException("Invalid! Empty input!");
        }
    }
}
