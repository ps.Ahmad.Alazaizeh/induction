package com.induction.ps;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        EquationProcessor EP = new EquationProcessor(getUserInput());
        System.out.println("Result: "+EP.calculate());
    }

    private static String getUserInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter equation: ");
        return scanner.nextLine();
    }
}
