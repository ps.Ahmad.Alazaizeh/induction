package com.induction.ps;

public class Node {

    private Integer data = null;
    private Node left;
    private Node right;

    public boolean accept(int value) {
        if (data == null) {
            data = value;
        }
        if (value == data) {
            return false;
        }
        if (value > data) {
            if (right == null) {
                Node node = new Node();
                node.accept(value);
                right = node;
                return true;
            }
            return right.accept(value);
        } else {
            if (left == null) {
                Node node = new Node();
                node.accept(value);
                left = node;
                return true;
            }
            return left.accept(value);
        }
    }

    public Integer getData() {
        return data;
    }

    public Node getRight() {
        return right;
    }

    public Node getLeft() {
        return left;
    }
}