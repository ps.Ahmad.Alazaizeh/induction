package com.induction.ps;

public class BinaryTree {

    private final Node root = new Node();

    public BinaryTree(int rootValue) {
        root.accept(rootValue);
    }

    public boolean accept(int value) {
        return root.accept(value);
    }

    public int getDepth(int value) {
        Node searchNode = root;
        int depth = 0;
        while (searchNode.getData() != value) {
            if (value == searchNode.getData()) break;
            if (value > searchNode.getData()) {
                searchNode = searchNode.getRight();
            } else {
                searchNode = searchNode.getLeft();
            }
            depth++;
        }
        return depth;
    }

    public int getTreeDepth(Node node) {
        if (node == null) return -1;
        return Math.max(getTreeDepth(node.getLeft()), getTreeDepth(node.getRight())) + 1;
    }

    public Node getRoot() {
        return root;
    }
}
