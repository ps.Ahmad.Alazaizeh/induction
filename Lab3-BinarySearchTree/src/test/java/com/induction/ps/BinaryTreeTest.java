package com.induction.ps;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BinaryTreeTest {

    @Test
    public void givenValidTreeCheck_whenAccept_thenSuccess() {
        BinaryTree BT = new BinaryTree(5);
        BT.accept(3);
        BT.accept(4);
        BT.accept(6);
        BT.accept(8);
        BT.accept(2);
        BT.accept(9);
        BT.accept(7);
        BT.accept(1);

        int[] expectedRightArray = {5, 6, 8, 9};
        Node tempNode = BT.getRoot();
        int i = 0;
        while (tempNode != null && tempNode.getData() != null) {
            Assertions.assertEquals(expectedRightArray[i++], tempNode.getData(), "Wrong values at the right side!");
            tempNode = tempNode.getRight();
        }

        int[] expectedLeftArray = {5, 3, 2, 1};
        tempNode = BT.getRoot();
        int j = 0;
        while (tempNode != null && tempNode.getData() != null) {
            Assertions.assertEquals(expectedLeftArray[j++], tempNode.getData(), "Wrong values at the left side!");
            tempNode = tempNode.getLeft();
        }
    }

    @Test
    public void givenValidTreeCheck_whenGetDepth_thenSuccess() {
        BinaryTree BT = new BinaryTree(5);
        BT.accept(3);
        BT.accept(4);
        BT.accept(6);
        BT.accept(8);
        BT.accept(2);
        BT.accept(9);
        BT.accept(7);
        BT.accept(1);
        BT.accept(0);

        Assertions.assertEquals(3, BT.getDepth(9), "Wrong depth!");
        Assertions.assertEquals(4, BT.getDepth(0), "Wrong depth!");
        Assertions.assertEquals(0, BT.getDepth(5), "Wrong depth!");
        Assertions.assertEquals(2, BT.getDepth(4), "Wrong depth!");
        Assertions.assertEquals(2, BT.getDepth(2), "Wrong depth!");
    }

    @Test
    public void givenValidTreeCheck_whenGetTreeDepth_thenSuccess() {
        BinaryTree BT = new BinaryTree(5);
        BT.accept(3);
        BT.accept(4);
        BT.accept(6);
        BT.accept(8);
        BT.accept(2);
        BT.accept(9);
        BT.accept(7);
        BT.accept(1);
        BT.accept(0);

        Assertions.assertEquals(4, BT.getTreeDepth(BT.getRoot()), "Wrong tree depth!");
    }

    @Test
    public void givenValidTreeButDuplicate_whenAccept_thenThrowException() {
        BinaryTree BT = new BinaryTree(5);
        Assertions.assertFalse(BT.accept(5), "Invalid! Duplicate value!");
    }
}
