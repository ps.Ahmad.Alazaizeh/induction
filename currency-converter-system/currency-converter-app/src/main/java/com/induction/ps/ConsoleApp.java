package com.induction.ps;

import com.induction.ps.CSVRateSupplierBehaviour;
import com.induction.ps.CurrencyConverter;
import com.induction.ps.RateSupplier;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Scanner;

public class ConsoleApp {

    CurrencyConverter currencyConverter;

    ConsoleApp(RateSupplier rs){
        currencyConverter = new CurrencyConverter(rs);
    }

    public void run(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter currency code to convert from: ");
        String from = scanner.nextLine();
        System.out.println("Please enter currency code to convert to: ");
        String to = scanner.nextLine();
        System.out.println("Please enter amount to be converted: ");
        BigDecimal amount = new BigDecimal(scanner.nextLine());
        try {
            System.out.println("Converted amount: " + currencyConverter.convert(from, to, amount));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
