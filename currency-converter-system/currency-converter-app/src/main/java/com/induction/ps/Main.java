package com.induction.ps;

public class Main {
    public static void main(String[] args) {

        final String pathString = System.getProperty("path");
        final String fileName = System.getProperty("fileName");
        final String frequency = System.getProperty("frequency");
        final String collection = System.getProperty("collection");

        System.out.println(pathString);
        System.out.println(fileName);
        System.out.println(frequency);
        System.out.println(collection);

        if (pathString == null || fileName == null || frequency == null || collection == null){
            System.out.println("Please provide path, file name, frequency, collection.");
            return;
        }

        CSVRateSupplierBehaviour rs = new CSVRateSupplierBehaviour(pathString, fileName, frequency, collection);
        ConsoleApp consoleApp = new ConsoleApp(rs);
        consoleApp.run();
    }
}
