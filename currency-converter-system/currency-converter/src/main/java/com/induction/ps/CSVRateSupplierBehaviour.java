package com.induction.ps;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CSVRateSupplierBehaviour implements RateSupplier {

    CSVReaderUtility csvReaderUtility = new CSVReaderUtility();

    private final Path path;
    private final String fileName;
    private final String frequency;
    private final String collection;

    public CSVRateSupplierBehaviour(String pathString, String fileName, String frequency, String collection){
        this.path = Paths.get(pathString);
        this.fileName = fileName;
        this.frequency = frequency;
        this.collection = collection;
    }

    @Override
    public BigDecimal getRate(String code){
        File file = path.resolve(fileName).toFile();
        try {
            return csvReaderUtility.read(file, frequency, code, collection);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
