package com.induction.ps;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class CurrencyConverter {

    RateSupplier rateSupplier;

    public CurrencyConverter(RateSupplier rateSupplier){
        this.rateSupplier = rateSupplier;
    }

    public BigDecimal convert(String from, String to, BigDecimal amount) throws IOException {
        validateCurrencyCodes(from, to);
        validateDuplicatedCodes(from, to);
        validateAmount(amount);
        BigDecimal result = amount;
        if (!checkIfUSD(from)){
            result = convertToUSD(from, amount);
        }
        return convertFromUSD(to, result);
    }

    private boolean checkIfUSD(String code) {
        return code.equals("USD");
    }

    private BigDecimal convertFromUSD(String to, BigDecimal amount) throws IOException {
        if (checkIfUSD(to)){
            return amount;
        }
        return amount.multiply(rateSupplier.getRate(to));
    }

    private BigDecimal convertToUSD(String from, BigDecimal amount) throws IOException {
        return amount.divide(rateSupplier.getRate(from), 2, RoundingMode.HALF_EVEN);
    }

    private void validateAmount(BigDecimal amount) {
        if (amount.equals(BigDecimal.valueOf(0))){
            throw new IllegalArgumentException("Invalid! Amount is zero!");
        }
        if (amount.doubleValue() < 0){
            throw new IllegalArgumentException("Invalid! Amount is negative!");
        }
    }

    private void validateDuplicatedCodes(String from, String to) {
        if (from.equals(to)){
            throw new IllegalArgumentException("Invalid! Codes are the same!");
        }
    }

    private void validateCurrencyCodes(String from, String to) {
        if (from == null){
            throw new IllegalArgumentException("Invalid! From code is null!");
        }
        if (to == null){
            throw new IllegalArgumentException("Invalid! To code is null!");
        }
    }
}
