package org.induction;

import org.induction.ps.Stack;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StackTest {
    @Test
    public void givenInvalidStackZeroCapacity_whenStackConstructed_thenThrowException() {
        int capacity = 0;
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new Stack<String>(capacity));
        Assertions.assertEquals("Invalid capacity provided: 0!", exception.getMessage());
    }

    @Test
    public void givenValidStackCheckForSizeAndElements_whenPushAndPop_thenSuccess() {
        String elementOne = "Test String One";
        String elementTwo = "Test String Two";
        Stack<String> stack = new Stack<>();

        stack.push(elementOne);
        Assertions.assertEquals(1, stack.size(), "Wrong size after push!");
        stack.push(elementTwo);
        Assertions.assertEquals(2, stack.size(), "Wrong size after push!");

        Object popedStringOne = stack.pop();
        Assertions.assertEquals("Test String Two", popedStringOne, "Wrong element poped!");
        Object popedStringTwo = stack.pop();
        Assertions.assertEquals("Test String One", popedStringTwo, "Wrong element poped!");
    }

    @Test
    public void givenValidStackButNullInput_whenPush_thenThrowException() {
        Stack<String> stack = new Stack<>();

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> stack.push(null));
        Assertions.assertEquals("Invalid! Null input!", exception.getMessage());
    }

    @Test
    public void givenValidStackButFull_whenPush_thenThrowException() {
        Stack<String> stack = new Stack<>(1);
        stack.push("Test String");

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> stack.push("Test String 2"));
        Assertions.assertEquals("Invalid! Full Stack!", exception.getMessage());
    }

    @Test
    public void givenValidStackButEmpty_whenPop_thenThrowException() {
        Stack<String> stack = new Stack<>();
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, stack::pop);
        Assertions.assertEquals("Invalid! Empty stack!", exception.getMessage());
    }

    @Test
    public void givenValidStackCheckForSizeAndElements_whenPeek_thenSuccess() {
        Stack<String> stack = new Stack<>();
        stack.push("Test String One");
        stack.push("Test String Two");

        Object peekedElementOne = stack.peek();
        Assertions.assertEquals(2, stack.size(), "Wrong size after peek!");
        Assertions.assertEquals("Test String Two", peekedElementOne, "Wrong element poped!");
        stack.pop();
        Object peekedElementTwo = stack.peek();
        Assertions.assertEquals(1, stack.size(), "Wrong size after peek!");
        Assertions.assertEquals("Test String One", peekedElementTwo, "Wrong element poped!");
    }

    @Test
    public void givenValidStackButEmpty_whenPeek_thenThrowException() {
        Stack<String> stack = new Stack<>();
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, stack::peek);
        Assertions.assertEquals("Invalid! Empty stack!", exception.getMessage());
    }

}
