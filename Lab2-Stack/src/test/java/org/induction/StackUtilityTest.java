package org.induction;

import org.induction.ps.Stack;
import org.induction.ps.StackUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StackUtilityTest {
    @Test
    public void givenValidGenericStack_whenMove_thenSuccess(){
        Stack<String> stackFrom = new Stack<>();
        Stack<String> stackTo = new Stack<>();
        stackFrom.push("One");
        stackFrom.push("Two");
        stackFrom.push("Three");

        StackUtility<String> stackUtility = new StackUtility<>();
        stackUtility.move(stackFrom, stackTo);

        Assertions.assertEquals("One", stackTo.pop(), "Invalid! Not equal value");
        Assertions.assertEquals("Two", stackTo.pop(), "Invalid! Not equal value");
        Assertions.assertEquals("Three", stackTo.pop(), "Invalid! Not equal value");
    }

    @Test
    public void givenValidStackButEmpty_whenPeek_thenThrowException() {
        Stack<String> stackFrom = new Stack<>();
        Stack<String> stackTo = new Stack<>();
        StackUtility<String> stackUtility = new StackUtility<>();
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, ()-> stackUtility.move(stackFrom, stackTo));
        Assertions.assertEquals("Invalid! Empty stack!", exception.getMessage());
    }
}
