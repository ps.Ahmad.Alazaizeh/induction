package org.induction;

import org.induction.ps.Queue;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class QueueTest {
    @Test
    public void givenInvalidQueueZeroCapacity_whenQueueConstructed_thenThrowException() {
        int capacity = 0;
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new Queue<String>(capacity));
        Assertions.assertEquals("Invalid capacity provided: 0!", exception.getMessage());
    }

    @Test
    public void givenValidQueueCheckForSizeAndElements_whenEnqueAndDeque_thenSuccess() {
        String elementOne = "Test String One";
        String elementTwo = "Test String Two";
        Queue<String> queue = new Queue<>();

        queue.enque(elementOne);
        Assertions.assertEquals(1, queue.size(), "Wrong size after enque!");
        queue.enque(elementTwo);
        Assertions.assertEquals(2, queue.size(), "Wrong size after enque!");

        Object dequedStringOne = queue.deque();
        Assertions.assertEquals("Test String One", dequedStringOne, "Wrong element deque!");
        Object dequedStringTwo = queue.deque();
        Assertions.assertEquals("Test String Two", dequedStringTwo, "Wrong element deque!");
    }

    @Test
    public void givenValidQueueButNullInput_whenConstructed_thenThrowException() {
        Queue<String> queue = new Queue<>();
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> queue.enque(null));
        Assertions.assertEquals("Invalid! Null input!", exception.getMessage());
    }

    @Test
    public void givenValidQueueButFull_whenEnque_thenThrowException() {
        Queue<String> queue = new Queue<>(1);
        queue.enque("Test String");

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> queue.enque("Test String 2"));
        Assertions.assertEquals("Invalid! Full Queue!", exception.getMessage());
    }

    @Test
    public void givenValidQueueButEmpty_whenDeque_thenThrowException() {
        Queue<String> queue = new Queue<>();
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, queue::deque);
        Assertions.assertEquals("Invalid! Empty Queue!", exception.getMessage());
    }

    @Test
    public void givenValidQueueCheckForSizeAndElements_whenPeek_thenSuccess() {
        Queue<String> queue = new Queue<>();
        queue.enque("Test String One");
        queue.enque("Test String Two");

        Object peekedElementOne = queue.peek();
        Assertions.assertEquals(2, queue.size(), "Wrong size after peek!");
        Assertions.assertEquals("Test String One", peekedElementOne, "Wrong element dequed!");
        queue.deque();
        Object peekedElementTwo = queue.peek();
        Assertions.assertEquals(1, queue.size(), "Wrong size after peek!");
        Assertions.assertEquals("Test String Two", peekedElementTwo, "Wrong element dequed!");
    }

    @Test
    public void givenValidQueueButEmpty_whenPeek_thenThrowException() {
        Queue<String> queue = new Queue<>();
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, queue::peek);
        Assertions.assertEquals("Invalid! Empty Queue!", exception.getMessage());
    }
}
