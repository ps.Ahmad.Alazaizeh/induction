package org.induction.ps;

import java.util.ArrayList;

public class Queue<T> {

    private final int capacity;
    private final ArrayList<T> arrayList;
    int head = 0;
    int rear = 0;

    public Queue() {
        capacity = -1;
        arrayList = new ArrayList<>();
    }

    public Queue(int capacity) {
        checkIfZeroCapacity(capacity);
        this.capacity = capacity;
        arrayList = new ArrayList<>(capacity);
    }

    public void enque(T element) {
        checkIfNullInput(element);
        if(!checkIfDynamic()){
            checkIfFull();
        }
        arrayList.add(element);
        rear++;
    }

    private boolean checkIfDynamic() {
        return capacity == -1;
    }

    public T deque() {
        checkIfEmpty();
        rear--;
        T element = arrayList.get(head);
        arrayList.set(head++, null);
        return element;
    }

    public T peek() {
        checkIfEmpty();
        return arrayList.get(head);
    }

    public int size() {
        return rear;
    }

    private void checkIfZeroCapacity(int capacity) {
        if (capacity == 0) {
            throw new IllegalArgumentException("Invalid capacity provided: 0!");
        }
    }

    private void checkIfEmpty() {
        if (rear == 0) {
            throw new IllegalArgumentException("Invalid! Empty Queue!");
        }
    }

    private void checkIfFull() {
        if (rear == capacity) {
            throw new IllegalArgumentException("Invalid! Full Queue!");
        }
    }

    private void checkIfNullInput(T element) {
        if (element == null) {
            throw new IllegalArgumentException("Invalid! Null input!");
        }
    }
}
