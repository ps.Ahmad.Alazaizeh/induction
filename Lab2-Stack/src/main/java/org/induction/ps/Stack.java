package org.induction.ps;

import java.util.ArrayList;

public class Stack<T> {

    private final int capacity;
    private final ArrayList<T> arrayList;
    private int top = 0;

    public Stack() {
        capacity = -1;
        arrayList = new ArrayList<>();
    }

    public Stack(int capacity) {
        checkIfZeroCapacity(capacity);
        this.capacity = capacity;
        arrayList = new ArrayList<>(capacity);
    }

    public void push(T element) {
        checkIfNullInput(element);
        if (!checkIfDynamic()){
            checkIfFull();
        }
        arrayList.add(element);
        top++;
    }

    private boolean checkIfDynamic() {
        return capacity == -1;
    }

    public T pop() {
        checkIfEmpty();
        top--;
        T element = arrayList.get(top);
        arrayList.set(top, null);
        return element;
    }

    public T peek() {
        checkIfEmpty();
        int tempSize = top - 1;
        return arrayList.get(tempSize);
    }

    public int size() {
        return top;
    }

    private void checkIfZeroCapacity(int capacity) {
        if (capacity == 0) {
            throw new IllegalArgumentException("Invalid capacity provided: 0!");
        }
    }

    private void checkIfEmpty() {
        if (top == 0) {
            throw new IllegalArgumentException("Invalid! Empty stack!");
        }
    }

    private void checkIfFull() {
        if (top == capacity) {
            throw new IllegalArgumentException("Invalid! Full Stack!");
        }
    }

    private void checkIfNullInput(T element) {
        if (element == null) {
            throw new IllegalArgumentException("Invalid! Null input!");
        }
    }
}
