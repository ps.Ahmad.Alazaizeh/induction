package org.induction.ps;

public class StackUtility<T> {
    public void move(Stack<? extends T> from, Stack<? super T> to){
        checkIfEmpty(from);
        while (from.size() > 0){
            to.push(from.pop());
        }
    }

    private void checkIfEmpty(Stack<? extends T> from) {
        if (from.size() == 0){
            throw new IllegalArgumentException("Invalid! Empty stack!");
        }
    }
}
